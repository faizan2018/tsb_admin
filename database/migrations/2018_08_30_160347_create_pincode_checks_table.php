<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePincodeChecksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pincode_checks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product');
            $table->integer('pincode');
            $table->string('city');
            $table->string('state');
            $table->string('region');
            $table->boolean('prepaid');
            $table->boolean('cod');
            $table->boolean('reversepickup');
            $table->boolean('pickup');
            $table->string('serviceable_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pincode_checks');
    }
}
