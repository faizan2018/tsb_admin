<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactSolutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('contact_solutions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('contact_form_id');
            $table->foreign('contact_form_id')->references('id')->on('contacts');
            $table->string('description',10000)->nullable()->default(NULL);
            $table->boolean('under_process')->default(false);
            $table->boolean('handled')->default(false);
            $table->unsignedInteger('admin_id')->nullable()->default(NULL);
            $table->foreign('admin_id')->references('id')->on('admins');
            $table->timestamps();
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('contact_solutions');
        Schema::enableForeignKeyConstraints();
    }
}
