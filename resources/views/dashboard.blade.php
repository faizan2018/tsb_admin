@extends('layouts.headerAndFooter')
@section('content')

	<div class="container font-opensans">
		<div class="row" id="row-buttons">
			<div class="col col-12 col-sm-12 col-lg-3">
				<a href="" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Manage Products</a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="addProduct">Add a Product</a>
						<a class="dropdown-item" href="#">List Products</a>

						<div role="separator" class="dropdown-divider"></div>

						<a class="dropdown-item" href="manageCategories">Categories</a>
						<a class="dropdown-item" href="manageSubCategories">Sub-Categories</a>
						<a class="dropdown-item" href="manageSubCategoryVersions">Sub-Category Version</a>

						<div role="separator" class="dropdown-divider"></div>

						<a href="" class="dropdown-item dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Attribute Set</a>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="attributeNames">Attribute Names</a>
								<a class="dropdown-item" href="attributeTypes">Attribute Types</a>
								<a class="dropdown-item" href="attributeSetName">Attribute Set Name</a>
								<a class="dropdown-item" href="">Attribute Values</a>
							</div>
					</div>
				<a href="orders" type="button" class="btn btn-primary">List Orders</a>
			</div>
			<div class="col col-12 col-sm-12 col-lg-3">
				<a href="contacts" type="button" class="btn btn-primary">Contact Submissions</a>
				<a href="manualOrder" type="button" class="btn btn-primary">Manual Order</a>
			</div>
			<div class="col col-12 col-sm-12 col-lg-3">
				<a href="subscribers" type="button" class="btn btn-primary">Subscriber List</a>
			</div>
			<div class="col col-12 col-sm-12 col-lg-3">
				<a href="configs" type="button" class="btn btn-primary">Set Configuration</a>
				<a href="support" type="button" class="btn btn-primary">Support</a>
			</div>
		</div>
		<!--<iframe name="Framename" src="http://www.hashpeers.com/" frameborder="0" scrolling="no" style="width: 100%;"></iframe>-->
	</div> 

@endsection