@extends('layouts.headerAndFooter')

@section('content')

<div class="container font-opensans">
	<div class="row">
		<div class="col col-12 col-md-12 col-lg-6">
			<div class="title">
				<h5>Add a Category</h5>
				<hr align="left">
			</div>
			<form class="pad-rl-15" method="post" action="@if($category ?? FALSE) {{route('updateCategory',$category->id)}} @else {{route('addCategory')}} @endif">
				@csrf
				<div class="form-group row">
					<div class="col col-12 col-sm-6 col-md-6 pad-5">
						<input class="form-control" value="@if($category ?? FALSE) {{$category->category_name}} @endif" type="text" name="category_name" placeholder="Category name" required>
					</div>
					<div class="col col-4 col-sm-6 offset-md-1 col-md-4 pad-5">
						<input class="form-control btn btn-primary" type="submit" value="@if($category ?? FALSE) Update @else Add @endif">
					</div>
				</div>				
			</form>
			@if(session('success'))
				<div class="alert alert-success alert-dismissible" role="alert">
					{{session('success')}}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@elseif(session('failure'))
				<div class="alert alert-danger alert-dismissible" role="alert">
					{{session('failure')}}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@elseif(session('warning'))
				<div class="alert alert-warning alert-dismissible" role="alert">
					{{session('warning')}}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@endif
		</div>
		<div class="col col-12 col-md-12 col-lg-6">
			<div class="title">
				<h5>Category Table</h5>
				<hr align="left">
			</div>
			@if($categories ?? FALSE)
			<?php $count = 0; ?>
				<div class="table-responsive">
					<table class="table table-hover table-bordered">
						
						<thead class="thead-dark">
							<tr>
								<th scope="col">#</th>
								<th scope="col">Category name</th>
								<th scope="col" colspan="2">Action</th>
							</tr>
							
						</thead>
						<tbody>
							@foreach($categories as $category)
							<?php $count++; ?>
							<tr class="table-light">
								<td>{{$count}}</td>
								<td>{{$category->category_name}}</td>
								<td><button type="button" class="btn btn-warning customised" onclick="location.href='/editCategory/'+{{$category->id}}">Edit</button></td>
								<td><button type="button" class="btn btn-danger customised" onclick="location.href='/deleteCategory/'+{{$category->id}}">Delete</button></td>
							</tr>
							@endforeach
						</tbody>	
					</table>
				</div>
				@else
				No entry found in this list
			@endif
		</div>
	</div>
</div>

@endsection




