@extends('layouts.headerAndFooter')
@section('content')

<div class="container font-opensans">
	<div class="title">
		<h5>Subscriptions List</h5>
		<hr align="left">
	</div>
	@if($subscribers ?? FALSE)

		<div class="table-responsive">

		<table id="subscribers-table" class="table table-striped table-hover table-bordered">
			<thead class="thead-dark">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Name</th>
					<th scope="col">Email</th>
					<th>Date & Time</th>
					<th scope="col">Selector</th>
				</tr>
			</thead>
			<tbody>
				<form method="post" action="{{ route('downloadcsv') }}">
					@csrf
					<?php $count = 0; ?>
					@foreach($subscribers as $subscriber)
					<?php $count++; ?>
					<tr>
						<th scope="row">{{$count}}</th>
						<td>
							@if($subscriber->name ??false)
								{{$subscriber->name}}
								@else
								Not a registered user
							@endif
						</td>
						<td>{{$subscriber->email}}</td>
						<td>{{date('d-m-Y',strtotime($subscriber->updated_at))}}
							<br>{{date('H:i:s',strtotime($subscriber->updated_at))}}
						</td>
						<td class="text-center tbl-min-wid">
							<input value="{{$subscriber->id}}" name="selected[]" class="checkbox" type="checkbox">
						</td>
					</tr>
					@endforeach
			</tbody>
		</table>
	</div>
		<button type="button" id="check" class="btn btn-primary subs">Select All</button>
		<button type="button" id="uncheck" class="btn btn-primary subs">Deselect All</button><br><br>

			<div class="pad-5">
				<b>Select data for CSV:</b>
				<br><br>
				<div class="form-group row">
					<div class="col col-12 col-sm-4">
						<input type="checkbox" id="full_name" name="full_name">
						&nbsp;&nbsp;
						<label for="full_name">Full Name</label>
						
					</div>

					<div class="col col-12 col-sm-4">
						<input type="checkbox" id="email" name="email" required>
						&nbsp;&nbsp;
						<label for="email">Email</label>
						
						
					</div>

					<div class="col col-12 col-sm-4">
						<input type="checkbox" id="datetime" name="datetime">
						&nbsp;&nbsp;
						<label for="datetime">Date & Time</label>
						
						
					</div>

					<div class="col col-12 col-sm-4">
						<input type="checkbox" id="all" name="all">
						&nbsp;&nbsp;
						<label for="all">All subscribers</label>
						
						
					</div>

					<div class="col col-12 col-sm-4">
						<input type="checkbox" id="registered" name="registered">
						&nbsp;&nbsp;
						<label for="registered">Registered subscribers only</label>
						
						
					</div>

					<div class="col col-12 col-sm-4">
						<input type="checkbox" id="non-registered" name="non-registered">
						&nbsp;&nbsp;
						<label for="non-registered">Non-registered subscribers only</label>
						
						
					</div>
					
					<div class="col col-12 col-sm-4">
						<input type="checkbox" id="status" name="status">
						&nbsp;&nbsp;
						<label for="status">Show subscription status in CSV</label>
						
						
					</div>
				</div>
			</div>
		<button type="submit" id="downloadCSV" class="btn btn-primary subs" disabled>Download CSV List</button>
		<br>
		<small>*Select at least one entry from the table to enable this button</small>

	</form>
	@else
		No record found
	@endif
</div>

@endsection