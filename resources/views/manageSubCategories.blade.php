@extends('layouts.headerAndFooter')

@section('content')
<div class="container font-opensans">
	<div class="row">

		<div class="col col-12 col-md-12 col-lg-6">
			<div class="title">
				<h5>Add a Sub-Category</h5>
				<hr align="left">
			</div>
			<form class="pad-rl-15" method="post" action="@if($sub_category ?? FALSE) {{route('updateSubCategory',$sub_category->id)}} @else {{route('addSubCategory')}} @endif">
				@csrf
				<div class="form-group row">
					<div class="col col-12 col-sm-12 col-md-6 pad-5">
						<input class="form-control" value="@if($sub_category ?? FALSE) {{$sub_category->sub_category_name}} @endif" type="text" name="sub_category_name" placeholder="Sub-Category Name" required>
					</div>
					<div class="col col-12 col-sm-12 col-md-6 pad-5">
						<select class="custom-select" name="category_id" required>
							@if($categories ?? FALSE)
								@foreach($categories as $category)
									<option value="{{$category->id}}">{{$category->category_name}}</option>
								@endforeach
							@else
								<option></option>
							@endif
						</select>
					</div>
					<div class="col col-6 col-sm-6 col-md-3 pad-5">
						<input type="submit" class="form-control btn btn-primary" value="@if($sub_category ?? FALSE) Update @else Add @endif">
					</div>
				</div>
			</form>
			@if(session('success'))
				<div class="alert alert-success alert-dismissible" role="alert">
					{{session('success')}}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@elseif(session('failure'))
				<div class="alert alert-danger alert-dismissible" role="alert">
					{{session('failure')}}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@elseif(session('warning'))
				<div class="alert alert-warning alert-dismissible" role="alert">
					{{session('warning')}}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@endif
		</div>

		<div class="col col-12 col-md-12 col-lg-6">
			<div class="title">
				<h5>Sub-Categories</h5>
				<hr align="left">
			</div>
			@if($sub_categories ?? FALSE)
				<?php $count = 0; ?>
				<div class="table-responsive">
					<table class="table table-hover table-bordered">
						<thead class="thead-dark">
							<tr>
								<th scope="col">#</th>
								<th scope="col">Sub Category name</th>
								<th scope="col">Category Name</th>
								<th scope="col" colspan="2">Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($sub_categories as $sub_category)
							<?php $count++; ?>
							<tr class="table-light">
								<td>{{$count}}</td>
								<td>{{$sub_category->sub_category_name}}</td>
								<td>{{$sub_category->categories->category_name}}</td>
								<td><button type="button" class="btn btn-warning customised" onclick="location.href='/editSubCategory/'+{{$sub_category->id}}">Edit</button></td>
								<td><button type="button" class="btn btn-danger customised" onclick="location.href='/deleteSubCategory/'+{{$sub_category->id}}">Delete</button></td>
							</tr>
							@endforeach
						</tbody>	
					</table>
				</div>
				@else
				No entry found in this list
			@endif
		</div>
	</div>
</div>

@endsection