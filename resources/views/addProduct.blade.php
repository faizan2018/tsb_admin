@extends('layouts.headerAndFooter')
@section('content')
<div class="container font-opensans">
	@if(session('success'))
		<div class="alert alert-success alert-dismissible" role="alert">
			{{session('success')}}
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
	@elseif(session('failure'))
		<div class="alert alert-danger alert-dismissible" role="alert">
			{{session('failure')}}
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
	@elseif(session('warning'))
		<div class="alert alert-warning alert-dismissible" role="alert">
			{{session('warning')}}
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
	@endif
	<form method="post" enctype="multipart/form-data" action="{{route('addNewProduct')}}">
		@csrf
		<div class="title">
			<h4>Product Details</h4>
			<hr align="left">
		</div>
		<div class="form-group row">

			<div class="col-lg-1"></div> <!-- Just for spacing-->

			<div class="col col-12 col-sm-12 col-lg">
				<label>Product Name</label>
				<input type="text" name="product_name" class="form-control" required>
			</div>
			<div class="col col-12 col-sm-12 offset-lg-1 col-lg">
				<label >Select Version</label>
				<select name="product_version" class="custom-select" required>
					@if($versions ?? false)
					@foreach($versions as $version)
						<option value="{{$version->id}}">{{$version->version_name}}</option>
					@endforeach
					@endif
				</select>
			</div>

			<div class="col-lg-1"></div> <!-- Just for spacing-->				
		</div>

		<div class="form-group row">
			<div class="col-lg-1"></div> <!-- Just for spacing-->

			<div class="col col-12 col-sm-12 col-lg">
				<label>Select Attribute Set</label>

				<select class="custom-select" id="attr-set" name="attribute_set" onchange="dynAddFields();" required>
					@if($attribute_set_names ?? false)
					@foreach($attribute_set_names as $attribute_set_name)
						<option value="{{$attribute_set_name->id}}">{{$attribute_set_name->attribute_set_name}}</option>
					@endforeach
					@endif
				</select>
			
			</div>

			<div class="col col-12 col-sm-12 offset-lg-1 col-lg">
				<label>Stock</label>
				<input type="text" class="form-control" name="stock" required>
			</div>

			<div class="col-lg-1"></div> <!-- Just for spacing-->
		</div>



		<div id="target-div"></div>


		<div class="form-group row">

			<div class="col-lg-1"></div> <!-- Just for spacing-->

			<div class="col col-12 col-sm-12 col-lg">

				<label>Select Images</label>
				<input type="file" class="form-control" accept="image/*" multiple name="product_images[]" multiple >

			</div>

			<div class="col col-12 col-sm-12 offset-lg-1 col-lg">
				<label>Product Sequence ID</label>
				<input type="text" class="form-control" name="product_sequence_id" required>
			</div>

			<div class="col-lg-1"></div> <!-- Just for spacing-->

		</div>

		<div class="form-group row mr-bt-50">

			<div class="col-lg-1"></div> <!-- Just for spacing-->

			<div class="col col-12 col-sm-12 col-lg">
				<label>New Label Start Date</label>
				<input type="date" class="form-control" name="product_new_label_start" required>
			</div>

			<div class="col col-12 col-sm-12 offset-lg-1 col-lg">
				<label>New Label End Date</label>
				<input type="date" class="form-control" name="product_new_label_end" required>
			</div>

			<div class="col-lg-1"></div> <!-- Just for spacing-->

		</div>

		<div class="title">
			<h4>Pricing</h4>
			<hr align="left">
		</div>

		<div class="form-group row">

			<div class="col-lg-1"></div> <!-- Just for spacing-->

			<div class="col col-12 col-sm-12 col-lg">
				<label>Actual Price</label>
				<input type="text" class="form-control" name="actual_price" required>
			</div>

			<div class="col col-12 col-sm-12 offset-lg-1 col-lg">
				<label>Special Price</label>
				<input type="text" class="form-control" name="special_price" required>
			</div>

			<div class="col-lg-1"></div> <!-- Just for spacing-->

		</div>

		<div class="form-group row mr-bt-50">

			<div class="col-lg-1"></div> <!-- Just for spacing-->

			<div class="col col-12 col-sm-12 col-lg">
				<label>Special Price Start Date</label>
				<input type="date" class="form-control" name="special_price_start" required>
			</div>

			<div class="col col-12 col-sm-12 offset-lg-1 col-lg">
				<label>Special Price End Date</label>
				<input type="date" class="form-control" name="special_price_end" required>
			</div>

			<div class="col-lg-1"></div> <!-- Just for spacing-->

		</div>

		<div class="title">
			<h4>SEO Handling</h4>
			<hr align="left">
		</div>

		<div class="form-group row">

			<div class="col-lg-1"></div> <!-- Just for spacing-->

			<div class="col col-12 col-sm-12 col-lg">
				<label>Title</label>
				<input type="text" class="form-control" name="seo_title" required>
			</div>

			<div class="col col-12 col-sm-12 offset-lg-1 col-lg">
				<label>Description</label>
				<input type="text" class="form-control" name="seo_description" required>
			</div>

			<div class="col-lg-1"></div> <!-- Just for spacing-->

		</div>

		<div class="form-group row mr-bt-50">

			<div class="col-lg-1"></div> <!-- Just for spacing-->

			<div class="col col-12 col-sm-12 col-lg">
				<label>Generator</label>
				<input type="text" class="form-control" name="seo_generator" required>
			</div>

			<div class="col col-12 col-sm-12 offset-lg-1 col-lg">
				<label>Keywords</label>
				<input type="text" class="form-control" name="seo_keywords" required>
			</div>

			<div class="col-lg-1"></div> <!-- Just for spacing-->

		</div>

		<div class="title">
			<h4>Coupons</h4>
			<hr align="left">
		</div>

		<div class="form-group row">

			<div class="col-lg-1"></div> <!-- Just for spacing-->

			<div class="col col-12 col-sm-12 col-lg">
				<label>Coupon Code</label>
				<input type="text" class="form-control" name="coupon_code" required>
			</div>

			<div class="col col-12 col-sm-12 offset-lg-1 col-lg">
				<label>Coupon Worth</label>
				<input type="text" class="form-control" name="coupon_worth" required>
			</div>

			<div class="col-lg-1"></div> <!-- Just for spacing-->

		</div>

		<div class="form-group row mr-bt-50">

			<div class="col-lg-1"></div> <!-- Just for spacing-->

			<div class="col col-12 col-sm-12 col-lg">
				<label>Coupon Start Date</label>
				<input type="date" class="form-control" name="coupon_start_date" required>
			</div>

			<div class="col col-12 col-sm-12 offset-lg-1 col-lg">
				<label>Coupon End Date</label>
				<input type="date" class="form-control" name="coupon_end_date" required>
			</div>

			<div class="col-lg-1">
				

			</div> <!-- Just for spacing-->

		</div>


		<div align="center">
			<input type="submit" id="addProduct" class="btn btn-danger" value="Add Product">
		</div>
	</form>

</div>
@endsection