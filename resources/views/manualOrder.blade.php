@extends('layouts.headerAndFooter')

@section('content')
	<div class="container">
		<h3>Add order</h3>
		<br>
		@if(session('success'))
				<div class="alert alert-success alert-dismissible" role="alert">
					{{session('success')}}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@elseif(session('failure'))
				<div class="alert alert-danger alert-dismissible" role="alert">
					{{session('failure')}}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@elseif(session('warning'))
				<div class="alert alert-warning alert-dismissible" role="alert">
					{{session('warning')}}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@endif
			<br>
		<form action="{{route('addOrder')}}" method="post">
			@csrf
			<div class="row">
				<div class="col-xs-12 col-md-3 col-sm-3">
					<div class="form-group">
						<label>Select user</label>
						<select name="name" class="form-control">
							@foreach($users as $user)
							<option value="{{$user->id}}">@if(isset($user->name)){{$user->name}}@else {{$user->email}} @endif</option>
							@endforeach
						</select>
					</div>

				</div>

				<div class="col-xs-12 col-md-43 col-sm-4">
					<div class="form-group">
						<label>Select product</label>
						<br>
							@foreach($products as $product)
						      <input name="product[]" value="{{$product->id}}" type="checkbox" aria-label="Checkbox for following text input">
						     <label style="padding: 10px;">{{$product->version->version_name}}</label> 
						     <br>
						     @endforeach
						     <!--  <input name="product[]" value="2" type="checkbox" aria-label="Checkbox for following text input">
						     <label style="padding: 10px;">Silicon Valley</label> --> 

					</div>
				</div>


				<div class="col-xs-12 col-md-2 col-sm-2">
					<div class="form-group">
						<label>Enter quantity</label>
							<br>
							<?php $count = 1; ?>
							@foreach($products as $product)
						     <input name="qty{{$count}}" type="text" class="form-control" aria-label="Text input with checkbox">
							<br>
							<?php $count++; ?>
						     @endforeach
						
					</div>
					<!-- <div class="form-group">
						     <input name="qty2" type="text" class="form-control" aria-label="Text input with checkbox">
					</div> -->
				</div>

				<div class="col-xs-12 col-md-3 col-sm-3">
						<div class="form-group">
						<label>Payment ID</label>
				
						     <input name="paymentId" type="text" class="form-control" aria-label="Text input with checkbox">
						
					</div>
					<div class="form-group">
						<label>Total Amount</label>
				
						     <input name="amount" type="number" class="form-control" aria-label="Text input with checkbox">
						
					</div>
				</div>

				<div class="offset-md-5">
					<br><br><br>
					<button class="btn btn-success" style="margin: auto;">Place Order</button>
					<br><br><br><br><br><br>
				</div>
				
			</div>
			
		</form>
	</div>
@endsection