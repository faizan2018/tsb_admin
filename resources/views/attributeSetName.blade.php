@extends('layouts.headerAndFooter')
@section('content')

<div class="container font-opensans">
	<div class="row">
		<div class="col col-12">
			<div class="title">
				<h5>Add Attribute Set</h5>
				<hr align="left">
			</div>
			<form class="pad-rl-15" action="{{route('addAttributeSetNames')}}" method="post">
				@csrf
				<div class="form-group row">
					<div class="col col-12 col-lg-3 pad-5">
						<div class="row">
							<div class="col col-12 col-md-6 col-lg-12">
								<input class="form-control" type="text" id="attr-new" name="attribute_set_name" placeholder="Attribute Set Name" required>
								<label class="col-form-label">Or choose from existing</label>
								&nbsp;&nbsp;
								<input name="existing" class="checkbox" id="attr-check" type="checkbox" unchecked="true">
							</div>
							<div class="col col-12 col-md-6 col-lg-12">
								<select name="exist_attribute_set_name" id="attr-old" class="form-control" disabled>
									@if($attribute_set_names ?? FALSE)
										@foreach($attribute_set_names as $attribute_name)
											<option value="{{$attribute_name->id}}">{{$attribute_name->attribute_set_name}}</option>
										@endforeach
									@else
									@endif
								</select>
							</div>
						</div>
						
						

					</div>

					<div class="col-lg"></div>
					<div class="col col-12 col-md-4 col-lg-2 pad-5">
						<label class="col-form-label">Text Field</label>
						&nbsp;&nbsp;

						<input name="text_field" class="checkbox" id="txt-chk" type="checkbox" unchecked="true" >

						<select name="text_attribute_name" id="attr-txt" class="form-control" disabled>
							@if($text_attribute_names ?? FALSE)
								@foreach($text_attribute_names as $attribute_name)
										<option value="{{$attribute_name->id}}">{{$attribute_name->attribute_name}}</option>
								@endforeach
							@else
							@endif
						</select>

					</div>
					
					<div class="col col-12 col-md-4 col-lg-2 pad-5">
						<label class="col-form-label">Varchar Field</label>
						&nbsp;&nbsp;

						<input name="varchar_field" class="checkbox" id="var-chk" type="checkbox" unchecked="true" >
						<select name="varchar_attribute_name" id="attr-var" class="form-control" disabled>
							@if($varchar_attribute_names ?? FALSE)
								@foreach($varchar_attribute_names as $attribute_name)
									<option value="{{$attribute_name->id}}">{{$attribute_name->attribute_name}}</option>
								@endforeach
							@else
							@endif
						</select>

					</div>
					
					<div class="col col-12 col-md-4 col-lg-2 pad-5">
						<label class="col-form-label">Decimal Field</label>
						&nbsp;&nbsp;

						<input name="decimal_field" class="checkbox" id="dec-chk" type="checkbox" unchecked="true" >

						<select name="decimal_attribute_name" id="attr-dec" class="form-control" disabled>
							@if($decimal_attribute_names ?? FALSE)
								@foreach($decimal_attribute_names as $attribute_name)
									<option value="{{$attribute_name->id}}">{{$attribute_name->attribute_name}}</option>
								@endforeach
							@else
							@endif
						</select>
					</div>

					<div class="col-lg"></div>

					<div class="col col-4 col-md-3 col-lg-2 pad-5" id="att-set-nm-btn">
						<input class="form-control btn btn-primary" value="Add" type="submit" required>
					</div>
				</div>
			</form>
		</div>
		<div class="col col-12">
			<div class="title">
				<h5>Attribute Set Table</h5>
				<hr align="left">
			</div>
			<div class="row">
				<div class="col offset-lg-3 col-lg-6">
					<div class="table-responsive">
						<table class="table table-hover table-bordered">
							
							<thead class="thead-dark">
								<tr>
									<th scope="col">#</th>
									<th scope="col">Attribute Set Name</th>
									<th scope="col" colspan="2">Action</th>
								</tr>
							</thead>
							<tbody>
								@if($attribute_set_names[0] ?? FALSE)
									@foreach($attribute_set_names as $attribute_set_name)
										<tr class="table-light">
											<td>1</td>
											<td>{{$attribute_set_name->attribute_set_name}}</td>
											<td><button type="button" class="btn btn-danger customised" onclick="location.href='/deleteAttributeSetName/'+{{$attribute_set_name->id}}" class="btn btn-danger">Delete</button></td>
										</tr>
									@endforeach
								@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
			
			
		</div>
	</div>
	
</div>

@endsection