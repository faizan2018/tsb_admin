@extends('layouts.headerAndFooter')

@section('content')

<div class="container font-opensans">	
	<div class="row">
		<div class="col col-12 col-md-12 col-lg-6">
			<div class="title">
				<h5>Add Attribute Type</h5>
				<hr align="left">
			</div>
			<form class="pad-rl-15" method="post" action="@if($attribute ?? FALSE){{route('updateAttributeTypes',$attribute->id)}} @else{{route('addAttributeTypes')}}@endif">
				@csrf
				<div class="form-group row">
					<div class="col col-12 col-sm-12 col-md-6 pad-5">
						<input class="form-control" value="@if($attr_type = $attribute->attribute_type ?? FALSE){{$attr_type}}@endif" type="text" name="attribute_type" placeholder="Attribute type" required>
					</div>
					<div class="col col-12 col-sm-12 col-md-6 pad-5">
						<input class="form-control" value="@if($attr_field = $attribute->field_type ?? FALSE){{$attr_field}}@endif" type="text" name="attribute_field" placeholder="Attribute field" required>
					</div>
					<div class="col col-6 col-sm-6 col-md-3 pad-5">
						<input class="form-control btn btn-primary" value="@if($attribute ?? FALSE)Update @else Add @endif" type="submit">
					</div>
				</div>
			</form>
			@if(session('success'))
				<div class="alert alert-success alert-dismissible" role="alert">
					{{session('success')}}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@elseif(session('failure'))
				<div class="alert alert-danger alert-dismissible" role="alert">
					{{session('failure')}}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@elseif(session('warning'))
				<div class="alert alert-warning alert-dismissible" role="alert">
					{{session('warning')}}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@endif
		</div>
		<div class="col col-12 col-md-12 col-lg-6">
			<div class="title">
				<h5>Attribute Type Table</h5>
				<hr align="left">
			</div>
			<div class="table-responsive">
				<table class="table table-hover table-bordered">
					<thead class="thead-dark">
						<tr>
							<th scope="col">#</th>
							<th scope="col">Attribute Type</th>
							<th scope="col">Attribute Field</th>
							<th scope="col" colspan="2">Action</th>
						</tr>	
					</thead>
					<tbody>
						@if($attributes[0] ?? FALSE)
						<?php $count = 0; ?>
							@foreach($attributes as $attribute)
							<?php $count++; ?>
							<tr class="table-light">
								<td>{{$count}}</td>
								<td>{{$attribute->attribute_type}}</td>
								<td>{{$attribute->field_type}}</td>
								<td><button type="button" class="btn btn-warning customised" onclick="location.href='/editAttributeType/'+{{$attribute->id}}">Edit</button></td>
								<td><button type="button" class="btn btn-danger customised" onclick="location.href='/deleteAttributeType/'+{{$attribute->id}}">Delete</button></td>
							</tr>
							@endforeach
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>


</div>


@endsection