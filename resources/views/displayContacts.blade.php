@extends('layouts.headerAndFooter')
@section('content')
<div class="container-fluid font-opensans">

  <div class="row">
    <div class="col-lg-1"></div>
    <div class="col">
      <div class="title">
        <h5>Contact Form Submissions</h5>
        <hr align="left">
      </div>
    </div>
    <div class="col-lg-1"></div>
  </div>

	@if($contacts ?? FALSE)
  <div class="table-responsive">
		<table id="contact-table" class="table table-hover table-bordered">
			<thead class="thead-dark">
				<tr>
			    <th class="text-center" scope="col">#</th>
					<th class="text-center" scope="col">Name</th>
					<th class="text-center" scope="col">Email</th>
					<th class="text-center" scope="col">Contact</th>
					<th class="text-center" scope="col">Message</th>
					<th class="text-center" scope="col">Date</th>
					<th class="text-center" scope="col">Handling Status</th>
					<th class="text-center" scope="col">Handling Option</th>
					<th class="text-center" scope="col">Description Last Updated</th>
					<th class="text-center" scope="col">Handled By</th>
				</tr>
			</thead>
			<tbody>
				<?php $count = 0; ?>
				  @foreach($contacts as $contact)
				    <?php $count++; ?>
            <tr class="table-light">
            <td>{{$count}}</td>
            <td>{{$contact->name}}</td>
            <td class="min-wid-150"><textarea class="txt-ar-cus" rows="3" readonly>{{$contact->email}}</textarea></td>
            <td class="wid-110">{{$contact->contact}}</td>
            <td class="min-wid-500"><textarea class="txt-ar-cus" rows="3" readonly>{{$contact->message}}</textarea></td>
            <td class="wid-110">
              {{date('d-m-Y',strtotime($contact->created_at))}}
              <br>
              {{date('H:i:s',strtotime($contact->created_at))}}
            </td>
            <td class="wid-110">
  						@if($contact->under_process ?? false)
  							<font class="u_p">Under Process</font>
  						@elseif($contact->handled ?? false)
  							<font class="h">Handled</font>
  						@else
  							<font class="u_h">Unhandled</font>	
  						@endif
            </td>

            @if($contact->under_process == false && $contact->admin_id == NULL)
            <td>
              <button class="btn btn-success customised" data-toggle="modal" data-target="#exampleModal{{$contact->id}}">
                Handle
              </button>
            </td>

            <div class="modal fade" id="exampleModal{{$contact->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Problem Handling Menu</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form action="{{route('addContactSolutionDescription',$contact->solution_id)}}" method="post">
                    @csrf
                    <div class="form-group">
                      <label>Enter the solution description</label>
                      <textarea class="form-control txt-ar-modal" rows="5" name="description"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                  </form>
              </div>
            </div>
          </div>
            @endif

  					@if($contact->under_process == true || $contact->handled == true)
  					@if(\Auth::user()->id == $contact->admin_id && $contact->handled == false)
  					<td class="wid-150">
              <button class="btn btn-success" data-toggle="modal" data-target="#exampleModal{{$contact->id}}">
                Manage
              </button>
            </td>


		        <div class="modal fade" id="exampleModal{{$contact->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Problem handling menu</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                	@if(!$contact->under_process ?? false)
                    <form action="{{route('addContactSolutionDescription',$contact->solution_id)}}" method="post">
                      @csrf
                   		<label>Enter the solution description</label>
                   		<textarea rows="10" name="description"></textarea>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                    </form>
  		            @else
              			<label><small>Your last updated description:</small></label><br>
              			<div id="view-box{{$contact->id}}">
              				<textarea class="txt-ar-cus" rows="5" readonly>{{$contact->description}}</textarea>
              			</div>
              			<div id="inputs-box{{$contact->id}}" hidden>
              				<form action="{{route('updateContactSolutionDescription',$contact->solution_id)}}" method="post">
              					@csrf
                        <div class="form-group">
                          <label>Enter the solution description</label>
                          <textarea class="form-control txt-ar-modal" rows="5" name="description">{{$contact->description}}</textarea>
                        </div>
              					<div class="form-group">
                          <button class="btn btn-danger" onclick="back(event, {{$contact->id}})">Cancel</button>
                          <button class="btn btn-primary">Save Changes</button>
                        </div>
              				</form>	
                    </div>
                      <button id="editButton" class="btn btn-primary mr-tb-5" onclick="changeToText({{$contact->id}});">
                        Edit description
                      </button>
                      <button class="btn btn-warning mr-tb-5" onclick="location.href='/releaseContactHandling/'+{{$contact->solution_id}}">
                        Release handling
                      </button>
                      <button class="btn btn-success mr-tb-5" onclick="location.href='/finalSubmitContactHandling/'+{{$contact->solution_id}}">
                        Final submit
                      </button>
                    </div>
              			
              		  @endif
                  </div>
                </div>
              </div>
              @else
                <td class="wid-150">
                  <button class="btn btn-warning customised" data-toggle="modal" data-target="#exampleModal{{$contact->id}}">
                    See description
                  </button>
                </td>



            	<div class="modal fade" id="exampleModal{{$contact->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Problem handling menu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                <div class="modal-body">
      	
              <label><small>Previous description:</small></label>
              <br>
              {{$contact->description}}
              <br><br>
            </div>
          </div>
        </div>


        @endif
        @endif


                <td class="wid-110">
                	@if($contact->under_process == true || $contact->handled == true)
                  	{{date('d-m-Y',strtotime($contact->contact_solutions_updated))}}
                    <br>
                  	{{date('H:i:s',strtotime($contact->contact_solutions_updated))}}
                  @else
                  	N/A
                	@endif
                </td>
                <td class="wid-110">
                	@if($contact->under_process == true || $contact->handled == true)
                  	@if(Auth::user()->id == $contact->admin_id)
                      You	
                  	@else
                  	 {{$contact->admin->name}}
                  	@endif
                  	@else
                  	N/A
                	@endif
                </td>
              </tr>
      				@endforeach
      			</tbody>
      		</table>
        </div>
      	@else
      		No one has contacted you yet
      	@endif
      </div>

@endsection
