@extends('layouts.headerAndFooter')

@section('content')
<div class="container font-opensans">
	<div class="row">
		<div class="col col-12 col-lg-6">
			<div class="title">
				<h4>Change Settings</h4>
				<hr align="left">
			</div>
			<form class="pad-rl-15">
				<div class="form-group row">
					<div class="col col-12 col-md-6 col-lg-6">
						<input type="text" class="form-control" name="" placeholder="GST">
					</div>
					<div class="col col-12 col-md-6 col-lg-6">
						<input type="text" class="form-control" name="" placeholder="Delivery Charges">
					</div>
				</div>
				<button type="submit" class="btn btn-danger">Update</button>
			</form>
		</div>
		<div class="col col-12 col-lg-6">
			<div class="title">
				<h4>Current Settings</h4>
				<hr align="left">
			</div>
			<div class="table-responsive">
				<table class="table table-hover table-bordered">
					<thead class="thead-dark">
						<tr>
							<th scope="col">#</th>
							<th scope="col">Name</th>
							<th scope="col">Value</th>
						</tr>
					</thead>
					<tbody>
						<tr class="table-light">
							<td>1</td>
							<td>Delivery Charges</td>
							<td>30</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection