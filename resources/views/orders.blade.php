@extends('layouts.headerAndFooter')

@section('content')
<div class="container font-opensans">
	<div class="row">
		<div class="title">
			<h4>Orders</h4>
			<hr align="left">
		</div>

		<!-- <form class="form-inline my-2 my-lg-0 ml-auto">
			<input class="form-control mr-sm-2" type="search" placeholder="Enter Order ID" aria-label="Search">
			<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search by Order ID</button>
		</form> -->
	</div>
	<!-- @if(session('success'))
				<div class="alert alert-success alert-dismissible" role="alert">
					{{session('success')}}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@elseif(session('failure'))
				<div class="alert alert-danger alert-dismissible" role="alert">
					{{session('failure')}}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@elseif(session('warning'))
				<div class="alert alert-warning alert-dismissible" role="alert">
					{{session('warning')}}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@endif -->
	<div class="row">
		
		<div class="table-responsive">
			<table class="table table-hover table-bordered">
				<thead class="thead-dark">
					<tr>
						<th scope="col">#</th>
						<th scope="col">Order Number</th>
						<th scope="col">Order Date</th>
						<th scope="col">Deliver to</th>
						<th scope="col">Delivery Address</th>
						<th scope="col">Contact Number</th>
						<th scope="col">Email</th>
						<th scope="col">Status</th>
						<th scope="col">Subtotal (INR)</th>
						<th scope="col">Tax (INR)</th>
						<th scope="col">Total (INR)</th>
					</tr>
				</thead>
				<tbody>
					<?php $count = 0; ?>
					@foreach($clubbedOrders as $orders)
					<?php $count++; ?>
					<tr class="table-light">
						
						<td >{{$count}}</td>
						<td >{{$orders[0]['order_no']}}</td>
						<td >{{$orders[0]['order_date']}}</td>
						<td >{{$orders[0]['ship_to']}}</td>
						<td >{{$orders[0]['address']}}</td>
						<td>{{$orders[0]['contact']}}</td>
						<td>{{$orders[0]['email']}}</td>
						<td>
							
							<p style="color: green;">Confirmed</p>
						
						</td>
					 
						<td>{{$orders[0]['total']}}</td>
						<td>{{$orders[0]['tax']}}</td>
						<td>{{$orders[0]['total']}}</td>
					</tr>
					<div align="right">
						<thead style="background-color: yellow;">
							<th></th>
							<th scope="col" colspan="3">Product Name</th>
							<th scope="col">Quantity</th>
							<th scope="col">Size</th>
							<th scope="col" colspan="2">Unit Price (INR)</th>
							<th scope="col" colspan="3">Total Price (INR)</th>
						</thead>
						@foreach($orders as $order)
						<tr class="table-light">
							<td></td>
							<td colspan="3">{{$order['product_name']}}</td>
							<td>{{$order['quantity']}}</td>
							<td>Free Size</td>	
							<td colspan="2">{{$order['unit_price']}}</td>
							<td colspan="3">{{$order['quantity']*$order['unit_price']}}</td>					
						</tr>

						@endforeach
						<thead style="background-color: yellow;">
							<th></th>
							<th scope="col" colspan="2">Dock No./ AWB No. / Tracking No.</th>
							<th scope="col" colspan="5">Reason</th>
							<th scope="col" colspan="5">Status</th>
						</thead>

						<tr class="table-light">
							<td></td>
							<td colspan="2">@if($orders[0]['truth']){{$orders[0]['dock_no']}}@endif</td>
							<td colspan="5">{{$orders[0]['reason']}}</td>
							<td colspan="5">@if($orders[0]['truth']){{$orders[0]['status']}}@endif</td>					
						</tr>

						<tr class="table-light">
							<td></td>
						</tr>
						<tr class="table-light">
							<td></td>
						</tr>
						<tr class="table-light">
							<td></td>
						</tr>
						<tr class="table-light">
							<td></td>
						</tr>
						<tr class="table-light">
							<td></td>
						</tr>	
					</div>
					
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection