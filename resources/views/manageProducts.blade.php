@extends('layouts.headerAndFooter')
@section('content')
<div class="container font-opensans">
		<div class="row" id="row-buttons">
			<div class="col col-12 col-sm-12 col-md-12 col-lg-3">
				<a href="" type="button" class="btn btn-primary">Attribute Set</a>
				<a href="" type="button" class="btn btn-primary">Add a Product</a>
			</div>
			<div class="col col-12 col-sm-12 col-md-12 col-lg-3">
				<a href="contacts" type="button" class="btn btn-primary">Categories</a>
				<a href="" type="button" class="btn btn-primary">List Products</a>
			</div>
			<div class="col col-12 col-sm-12 col-md-12 col-lg-3">
				<a href="subscribers" type="button" class="btn btn-primary">Sub-Categories</a>
			</div>
			<div class="col col-12 col-sm-12 col-md-12 col-lg-3">
				<a href="" type="button" class="btn btn-primary">Sub-Category Version</a>
			</div>
		</div>
	</div>
@endsection