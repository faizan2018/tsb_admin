@extends('layouts.headerAndFooter')

@section('content')
<div class="container font-opensans">

	<div class="title">
		<h5>Customer Support</h5>
		<hr align="left">
	</div>

	<ul class="nav nav-tabs" id="myTab" role="tablist">
		<li class="nav-item">
			<a class="nav-link active" id="create" data-toggle="tab" href="#create-case" role="tab" aria-controls="create-case" aria-selected="true">Create a case</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="show" data-toggle="tab" href="#show-cases" role="tab" aria-controls="show-cases" aria-selected="false">Show all cases</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="resolve" data-toggle="tab" href="#resolve-cases" role="tab" aria-controls="resolve-cases" aria-selected="false">Resolve cases</a>
		</li>
	</ul>

	<div class="tab-content" id="myTabContent">
		<div class="tab-pane fade show active" id="create-case" role="tabpanel" aria-labelledby="create">
			<form>
				
				<div class="row">
					<div class="col-lg-1"></div>
					<div class="col-lg title">
						<h6><b>Form for case creation</b></h6>
						<hr align="left">
					</div>
				</div>

				<div class="form-group row">
					<div class="col-lg-1"></div>

					<div class="col col-12 col-sm-12 col-lg">
						<label>Order ID</label>
						<input type="text" class="form-control">
					</div>

					<div class="col col-12 col-sm-12 offset-lg-1 col-lg">
						<label>Customer Name</label>
						<input type="text" class="form-control">
					</div>

					<div class="col-lg-1"></div>
				</div>

				<div class="form-group row">

					<div class="col-lg-1"></div>

					<div class="col col-12 col-sm-12 col-lg">
						<label>Mode of reaching out</label>
						<select class="form-control">
							<option>Phone</option>
							<option>Email</option>
						</select>
					</div>

					<div class="col col-12 col-sm-12 offset-lg-1 col-lg">
						<label>Contact details</label>
						<input type="text" class="form-control">
					</div>

					<div class="col-lg-1"></div>
				</div>

				<div class="form-group row">

					<div class="col-lg-1"></div>

					<div class="col col-12 col-sm-12 col-lg">
						<label>Issue type</label>
						<select class="form-control">
							<option>Delivery</option>
							<option>Product</option>
							<option>Coupon</option>
						</select>
					</div>

					<div class="col col-12 col-sm-12 offset-lg-1 col-lg">
						
					</div>

					<div class="col-lg-1"></div>
				</div>
				<div class="form-group row">
					<div class="col-lg-1"></div>
					<div class="col col-12 col-sm-12 col-lg">
						<label>Description</label>
						<textarea class="form-control no-res" rows="3"></textarea>
					</div>
					<div class="col-lg-1"></div>

					<div class="w-100"></div>

					<div class="col-lg-1"></div>
					<div class="col col-12 col-sm-12 col-lg">
						<label>Comments</label>
						<textarea class="form-control no-res" rows="3"></textarea>
					</div>
					<div class="col-lg-1"></div>
				</div>
				<div align="center">
					<button type="submit" class="btn btn-success">Create</button>
				</div>

			</form>

		</div>

		<div class="tab-pane fade" id="show-cases" role="tabpanel" aria-labelledby="show">
			<div class="row">

				<div class="col-lg title">
					<h6><b>All cases</b></h6>
					<hr align="left">
				</div>

				<div class="w-100"></div>

				<div class="col table-responsive">
					<table class="table table-hover table-bordered">
						<thead class="thead-dark">
							<tr>
								<th scope="col">#</th>
								<th scope="col">Time</th>
								<th scope="col">Order ID</th>
								<th scope="col">Customer Name</th>
								<th scope="col">Contacted Via</th>
								<th scope="col">Contact Details</th>
								<th scope="col">Issue Type</th>
								<th scope="col">Description</th>
								<th scope="col">Status</th>
								<th scope="col">Comments</th>
								<th scope="col">Handled by</th>
							</tr>	
						</thead>
						<tbody>
							<tr class="table-light">
								<td>1</td>
								<td>2</td>
								<td>3</td>
								<td>4</td>
								<td>5</td>
								<td>6</td>
								<td>7</td>
								<td>8</td>
								<td>9</td>
								<td>10</td>
								<td>11</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="tab-pane fade" id="resolve-cases" role="tabpanel" aria-labelledby="resolve">
			Ha Gulyo!<br>
			Cxe ma wuchthan yaar myoan?
		</div>
	</div>
</div>
@endsection