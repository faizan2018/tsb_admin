var countColor = 2;
var countSize = 2;
var checkBoxes = $('.checkbox');

checkBoxes.change(function () {
    $('#downloadCSV').prop('disabled', checkBoxes.filter(':checked').length < 1);
});

$(document).ready(function() {
	countColor = 2;
	countSize = 2;
    dynAddFields();
});

$('#check').click(function(){
	$('.checkbox').prop('checked', true);
	$('#downloadCSV').prop('disabled', checkBoxes.filter(':checked').length < 1);
});

$('#uncheck').click(function(){
	$('.checkbox').prop('checked', false);
	$('#downloadCSV').prop('disabled', checkBoxes.filter(':checked').length < 1);
});

$('#attr-check').click(function(){
   $('#attr-new').attr('disabled',this.checked);
   $('#attr-old').attr('disabled',!this.checked);
});

$('#txt-chk').click(function(){
   $('#attr-txt').attr('disabled',!this.checked);
});

$('#var-chk').click(function(){
   $('#attr-var').attr('disabled',!this.checked);
});

$('#dec-chk').click(function(){
   $('#attr-dec').attr('disabled',!this.checked);
});


function changeToText(id){
	$('#view-box'+id).hide();
	$('#editButton').hide();
	document.getElementById("inputs-box"+id).removeAttribute("hidden");
}
function back(event, id) {
	event.preventDefault();
	$('#editButton').css('display', 'block');
	$('#view-box'+id).css('display', 'block');
	$('#inputs-box'+id).attr('hidden', 'true');
}

function dynAddFields(){
	var attr_set = $('#attr-set').val();
	$.post('listAttributes',{attribute_set_name:attr_set},
      function(output){
      	$('.temp').remove();
      	for (var i = 0; i < output.length; i++) {
      		var row = "";
      		row = row+`
      		<div id="`+output[i]['attribute_name']+`">
				<div class="form-group row temp">
					<div class="col-lg-1"></div>	
			`;
			if (output[i]['range'] == true) {
				row = row+`
					<div class="col col-12 col-sm-12 col-lg">
						<label>`+output[i]['attribute_name']+`</label>
						<input type="text" class="form-control" name="decimal_start_range" placeholder="Start Range" required>
					</div>
					
					<div class="col col-12 col-sm-12 offset-lg-1 col-lg">
						<label>`+output[i]['attribute_name']+`</label>
						<input type="text" class="form-control" name="decimal_end_range" placeholder="End Range" required>
					</div>

					<input type="hidden" name="`+output[i]['attribute_name']+`[]" value="1">
				`;
			}
			else {
				row = row+`
					<div class="col col-12 col-sm-12 col-lg">
						<label>`+output[i]['attribute_name']+`</label>
						<input type="text" class="form-control" name="`+output[i]['attribute_name']+`[]" placeholder="Value" required>
					</div>
					`;
				if (output[i]['attribute_name'] == "Color" || output[i]['attribute_name'] == "Size") {
					row = row+`
					<div class="col col-12 col-sm-12 offset-lg-1 col-lg ajax-btn">
						<button type="button" id="`+output[i]['attribute_name']+`Add" onclick="add`+output[i]['attribute_name']+`Field('`+output[i]['attribute_name']+`');" class="btn btn-primary">Add more</button>
					</div>
				`;
				}
			}
			row = row+`
					<div class="col-lg-1"></div>
				</div>
			</div>
				<input value="`+output[i]['entity_id']+`" name="ent_id" hidden>
				<input value="`+output[i]['entity_type']+`" name="ent_typ" hidden>
			`;
			$('#target-div').append(row);
      	}

      });
	return false;
}


function addColorField(name){
	var row = `
		<div class="form-group row temp" id="Color`+countColor+`">
			<div class="col-lg-1"></div>

			<div class="col col-12 col-sm-12 col-lg">
				<label>Color `+countColor+`</label>
				<input type="text" class="form-control" name="`+name+`[]" placeholder="Value" required>
			</div>

			<div class="col col-12 col-sm-12 offset-lg-1 col-lg ajax-btn">
				<button type="button" class="btn btn-danger" onclick="remove(Color`+countColor+`);">Remove</button>
			</div>

			<div class="col-lg-1"></div>
		</div>

	`;
	$('#Color').append(row);
	countColor ++;
};

function addSizeField(name){
	var row = `
		<div class="form-group row temp" id="Size`+countSize+`">
			<div class="col-lg-1"></div>

			<div class="col col-12 col-sm-12 col-lg">
				<label>Size `+countSize+`</label>
				<input type="text" class="form-control" name="`+name+`[]" placeholder="Value" required>
			</div>

			<div class="col col-12 col-sm-12 offset-lg-1 col-lg ajax-btn">
				<button type="button" class="btn btn-danger" onclick="remove(Size`+countSize+`);">Remove</button>
			</div>

			<div class="col-lg-1"></div>
		</div>

	`;
	$('#Size').append(row);
	countSize ++;
};



function remove(target) {
	$(target).remove();
}