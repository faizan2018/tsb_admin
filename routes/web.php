<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::get('/contacts','ContactController@contactList')->name('contactList');
Route::get('/subscribers','SubscriptionController@listSubscribers')->name('subscriberList');
Route::get('/attributeTypes','AttributeController@listAttributeType')->name('listAttributeTypes');
Route::post('/addAttributeType','AttributeController@addAttributeType')->name('addAttributeTypes');
Route::get('/editAttributeType/{id}','AttributeController@editAttributeType')->name('editAttributeTypes');
Route::post('/updateAttributeType/{id}','AttributeController@updateAttributeType')->name('updateAttributeTypes');
Route::get('/deleteAttributeType/{id}','AttributeController@deleteAttributeType')->name('deleteAttributeTypes');
Route::get('/attributeNames','AttributeController@listAttributeName')->name('listAttributeNames');
Route::post('/addAttributeName','AttributeController@addAttributeName')->name('addAttributeNames');
Route::get('/editAttributeName/{id}','AttributeController@editAttributeName')->name('editAttributeNames');
Route::post('/updateAttributeName/{id}','AttributeController@updateAttributeName')->name('updateAttributeNames');
Route::get('/deleteAttributeName/{id}','AttributeController@deleteAttributeName')->name('deleteAttributeNames');
Route::post('/addAttributeSetName','AttributeController@addAttributeSetName')->name('addAttributeSetNames');
Route::get('/attributeSetName','AttributeController@listAttributeSetName')->name('listAttributeSetNames');
Route::get('/deleteAttributeSetName/{id}','AttributeController@deleteAttributeSetName')->name('deleteAttributeSetNames');



Route::get('/manageCategories','CategoryController@listCategory')->name('listCategories');
Route::post('/addCategory','CategoryController@addCategory')->name('addCategory');
Route::get('/editCategory/{id}','CategoryController@editCategory')->name('editCategory');
Route::post('/updateCategory/{id}','CategoryController@updateCategory')->name('updateCategory');
Route::get('/deleteCategory/{id}','CategoryController@deleteCategory')->name('deleteCategory');


Route::get('/manageSubCategories','SubCategoryController@listSubCategory')->name('listSubCategories');
Route::post('/addSubCategory','SubCategoryController@addSubCategory')->name('addSubCategory');
Route::get('/editSubCategory/{id}','SubCategoryController@editSubCategory')->name('editSubCategory');
Route::post('/updateSubCategory/{id}','SubCategoryController@updateSubCategory')->name('updateSubCategory');
Route::get('/deleteSubCategory/{id}','SubCategoryController@deleteSubCategory')->name('deleteSubCategory');


Route::get('/manageSubCategoryVersions','SubCategoryVersionController@listSubCategoryVersion')->name('listSubCategoryVersions');
Route::post('/addSubCategoryVersion','SubCategoryVersionController@addSubCategoryVersion')->name('addSubCategoryVersion');
Route::get('/editSubCategoryVersion/{id}','SubCategoryVersionController@editSubCategoryVersion')->name('editSubCategoryVersion');
Route::post('/updateSubCategoryVersion/{id}','SubCategoryVersionController@updateSubCategoryVersion')->name('updateSubCategoryVersion');
Route::get('/deleteSubCategoryVersion/{id}','SubCategoryVersionController@deleteSubCategoryVersion')->name('deleteSubCategoryVersion');



Route::get('/addProduct','ProductController@manageProduct')->name('addProduct');
Route::post('/addNewProduct','ProductController@addProduct')->name('addNewProduct');

Route::post('/listAttributes','AjaxController@listAttributes')->name('listAttributes');
Route::post('/addContactSolutionDescription/{id}','ContactSolutionController@addDescription')->name('addContactSolutionDescription');
Route::post('/updateContactSolutionDescription/{id}','ContactSolutionController@updateDescription')->name('updateContactSolutionDescription');
Route::get('/releaseContactHandling/{id}','ContactSolutionController@releaseHandling')->name('releaseContactHandling');
Route::get('/finalSubmitContactHandling/{id}','ContactSolutionController@finalSubmitHandling')->name('finalSubmitContactHandling');

Route::get('/orders','OrdersController@listOrders')->name('orders');
Route::get('/configs','SetConfigController@listConfigurations')->name('listConfigs');


Route::post('/downloadcsv','DownloadsController@downloadcsv')->name('downloadcsv');

//Route::get('/confirmOrder/{order_id}','OrdersController@confirmOrder')->name('confirmOrder');

Route::get('/track/{dock_no}','TestController@track');

Route::get('/manualOrder','OrdersController@manualOrder');
Route::post('/addOrder','OrdersController@addOrder')->name('addOrder');
//Route::post('/uploadPincodes','PincodeCheckController@uploadPincodes')->name('uploadPincodes');

//Route::get('/inputFile','PincodeCheckController@showInput')->name('inputFile');

