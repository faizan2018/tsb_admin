<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $fillable = [
        'order_id', 'total_order_price', 'confirm', 'payment_details_id',
    ];

    public function productSelection(){
    	return $this->hasMany('App\ProductSelection', 'order_id');
    }
}
