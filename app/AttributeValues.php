<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttributeValues extends Model
{
    //
    protected $fillable = [
        'text_field_value', 'varchar_field_value', 'decimal_field_value', 'decimal_start_range', 'decimal_end_range', 'text_entity_field_id', 'decimal_entity_field_id', 'varchar_entity_field_id'
    ];
}
