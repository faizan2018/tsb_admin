<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PincodeCheck extends Model
{
    //

    protected $fillable = [
    	'product', 'pincode', 'city', 'state', 'region', 'prepaid', 'cod', 'reversepickup', 'pickup', 'serviceable_by',
    ];
}
