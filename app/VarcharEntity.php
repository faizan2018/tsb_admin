<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VarcharEntity extends Model
{
    //
    protected $fillable = [
        'attribute_name_id', 'attribute_set_name_id',
    ];

    public function attribSetName(){
    	return $this->belongsTo('App\Attribute_Set_Name', 'attribute_set_name_id');
    }

    public function varcharAttribName(){
    	return $this->belongsTo('App\AttributeName', 'attribute_name_id');
    }
}
