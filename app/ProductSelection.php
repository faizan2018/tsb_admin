<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSelection extends Model
{
    //
    protected $filled = [
    	'coupon_applied', 'coupon_id', 'product_id', 'actual_price', 'special_price', 'user_id', 'delivery_details_id', 'order_id',
    ];

    public function user(){
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function delivery_detail(){
    	return $this->belongsTo('App\DeliveryDetail', 'delivery_details_id');
    }

    public function order(){
    	return $this->belongsTo('App\Order', 'order_id');
    }

    public function product_detail(){
        return $this->belongsTo('App\ProductCatalog', 'product_id');
    }

}
