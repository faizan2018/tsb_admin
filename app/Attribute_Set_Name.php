<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\AttributeName;

class Attribute_Set_Name extends Model
{
    //
    protected $fillable = [
    	'attribute_set_name','check_text_field','check_varchar_field', 'check_decimal_field',
    ];

    public function textfield(){
    	return $this->hasMany('App\TextEntity', 'attribute_set_name_id');
    }

    public function varcharfield(){
    	return $this->hasMany('App\VarcharEntity', 'attribute_set_name_id');
    }

    public function decimalfield(){
    	return $this->hasMany('App\DecimalEntity', 'attribute_set_name_id');
    }
}
