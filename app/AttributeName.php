<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttributeName extends Model
{
    //
    protected $fillable = [
        'attribute_name', 'attribute_type_id'
    ];

    public function attrib_type(){
    	return $this->belongsTo('App\AttributeType', 'attribute_type_id');
    }

    public function text(){
    	return $this->hasMany('App\TextEntity', 'attribute_name_id');
    }

    public function varchar(){
    	return $this->hasMany('App\VarcharEntity', 'attribute_name_id');
    }

    public function decimal(){
    	return $this->hasMany('App\DecimalEntity', 'attribute_name_id');
    }

}
