<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAttributesDetail extends Model
{
    //
    protected $fillable = [
    	'text_attr_value_id', 'varchar_attr_value_id', 'decimal_without_range_id', 'decimal_with_range_id', 'decimal_with_range_value', 'product_selections_id'
    ];	
}
