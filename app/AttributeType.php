<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttributeType extends Model
{
    //
    protected $fillable = [
    	'attribute_type', 'field_type'
    ];

    public function attrib_names(){
    	return $this->hasMany('App\AttributeName', 'attribute_type_id');
    }
  
}
