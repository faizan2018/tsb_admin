<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentModes extends Model
{
    //
    protected $fillable = [
    	'name',
    ];
}
