<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategoryVersion extends Model
{
    //
    protected $fillable = [
    	'version_name', 'sub_category_id'
    ];

    public function subCategories(){
    	return $this->belongsTo('App\SubCategory', 'sub_category_id');
    }
    public function catalog(){
    	return $this->hasMany('App\ProductCatalog', 'version_id');
    }
}
