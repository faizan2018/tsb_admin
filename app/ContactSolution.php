<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactSolution extends Model
{
    //
    protected $fillable = [
    	'contact_form_id','description','under_process','handled','admin_id'
    ];
    
    public function admin(){
    	return $this->belongsTo('App\Admin', 'admin_id');
    }
}
