<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentDetails extends Model
{
    //
    protected $fillable = [
    	'payment_mode_id', 'payment_id', 'payment_amount',
    ];
}
