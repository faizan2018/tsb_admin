<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCatalog extends Model
{
    //
    protected $fillable = [
    	'product_name', 'version_id', 'attribute_set_name_id', 'stock', 'product_image_directory_id', 'product_sequence_id', 'new_label_start_date', 'new_label_end_date', 'product_pricing_id', 'applicable_coupon_id', 'seo_id'
    ];

    public function productSelection(){
    	return $this->hasMany('App\Product_Selection', 'product_id');
    }

    public function version(){
    	return $this->belongsTo('App\SubCategoryVersion', 'version_id');
    }

    public function imageDir(){
    	return $this->belongsTo('App\ProductImageDirectory', 'product_image_directory_id');
    }

    public function pricing(){
    	return $this->belongsTo('App\Pricing', 'product_pricing_id');
    }
}
