<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    //
    protected $fillable = [
        'coupon_code', 'coupon_worth', 'coupon_start_date', 'coupon_end_date',
    ];
}
