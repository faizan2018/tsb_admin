<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImageDirectory extends Model
{
    //
    protected $fillable = [
    	'directory_url'
	];

	public function product(){
    	return $this->hasMany('App\ProductImageDirectory', 'product_image_directory_id');
    }
}
