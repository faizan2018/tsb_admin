<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $fillable = [
        'category_name'
    ];

    public function subCategories(){
    	return $this->hasMany('App\SubCategory', 'category_id');
    }
}
