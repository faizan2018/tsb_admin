<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pricing extends Model
{
    //
    protected $fillable = [
    	'actual_price', 'special_price', 'special_price_start_date', 'special_price_end_date'
    ];

     public function catalog(){
    	return $this->hasMany('App\ProductCatalog', 'product_pricing_id');
    }
}
