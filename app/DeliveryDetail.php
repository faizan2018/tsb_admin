<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryDetail extends Model
{
    //
    protected $fillable = [
    	'name', 'email', 'contact', 'address', 'city', 'state', 'country', 'pincode', 'user_id', 
	];

	public function productSelection(){
    	return $this->hasMany('App\ProductSelection', 'delivery_details_id');
    }
}
