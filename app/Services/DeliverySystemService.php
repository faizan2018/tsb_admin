<?php

namespace App\Services;
use App\ProductSelection;
use App\Order;
use App\UserAttributesDetail;
use App\Shipping;

class DeliverySystemService
{
    //
    //51
    //153
    public function confirmOrder($order_id){
            $order = Order::findOrFail($order_id);
        try{
            $product_selection = ProductSelection::where('order_id',$order_id)->first();
            $whole_order_data = ProductSelection::where('order_id',$order_id)->get();
            $quantity = 0;
            $weight = 200;
            foreach ($whole_order_data as $single_product_data) {
                $user_attributes = UserAttributesDetail::where('product_selections_id',$single_product_data->id)->first();
                $quantity = $user_attributes->decimal_with_range_value+$quantity; 
            }
            $weight = ($weight*$quantity)/1000;
            $pieces = ceil($quantity/2);
            $data = array(
                'Customer' => array(
                    'CUSTCD' => 'SP000102937'
                ),
                'DocketList' => array(
                    array(
                        'AgentID' => '',
                        'AwbNo' => '',
                        'Breath' => '5',
                        'CPD' => "".date('d/m/Y', strtotime('+7 days'))."",  //to be taken care of
                        'CollectableAmount' => "0",
                        'Consg_Number' => 'TSB'.$product_selection->order->order_id,
                        'Consolidate_EW' => '',
                        'CustomerName' => $product_selection->delivery_detail->name,
                        'Ewb_Number' => '',
                        'GST_REG_STATUS' => 'N',
                        'HSN_code' => '',
                        'Height' => '2.5',
                        'Invoice_Ref' => 'TSB'.$product_selection->order->order_id,
                        'IsPudo' => 'N',
                        'ItemName' => 'Socks '.$product_selection->order->order_id,
                        'Length' => '5',
                        'Mode' => 'P',
                        'NoOfPieces' => "".$pieces."",
                        'OrderConformation' => 'Y',
                        'OrderNo' => 'TSB'.$product_selection->order->order_id,
                        'ProductCode' => $product_selection->order->order_id,
                        'PudoId' => '',
                        'REASON_TRANSPORT' => '',
                        'RateCalculation' => 'N',
                        'Seller_GSTIN' => '',
                        'ShippingAdd1' => $product_selection->delivery_detail->address,
                        'ShippingAdd2' => $product_selection->delivery_detail->address,
                        'ShippingCity' => $product_selection->delivery_detail->city,
                        'ShippingEmailId' => $product_selection->delivery_detail->email,
                        'ShippingMobileNo' => substr($product_selection->delivery_detail->contact, -10),
                        'ShippingState' => $product_selection->delivery_detail->state,
                        'ShippingTelephoneNo' => substr($product_selection->delivery_detail->contact, -10),
                        'ShippingZip' => "".$product_selection->delivery_detail->pincode."",
                        'Shipping_GSTIN' => '',
                        'TotalAmount' => "".$product_selection->order->total_order_price."",
                        'TransDistance' => '',
                        'TransporterID' => '',
                        'TransporterName' => '',
                        'TypeOfDelivery' => 'Home Delivery',
                        'TypeOfService' => 'Economy',
                        'UOM' => 'Per KG',
                        'VendorAddress1' => '600, Sector 21, Panchkula',
                        'VendorAddress2' => '',
                        'VendorName' => 'The Socks Bakery',
                        'VendorPincode' => '134112',
                        'VendorTeleNo' => '8427299008',
                        'Weight' => "".$weight.""
                    )
                )
            );
            $Customer = $data;
            $headers = array('Content-Type: application/json');
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL,
            'https://instacom.dotzot.in/RestService/PushOrderDataService.svc/PushOrderData_PUDO_GST' );
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode($Customer) );
            $result = curl_exec($ch);
            curl_close($ch);
            $result1 = json_decode($result);
            $shipping = new Shipping();
            $shipping->dock_no = $result1[0]->DockNo;
            $shipping->order_no = $result1[0]->OrderNo;
            $shipping->reason = $result1[0]->Reason;
            $shipping->succeed = $result1[0]->Succeed;
            $shipping->order_id = $product_selection->order->order_id;
            $shipping->save();
            return $result;

        }
        catch (\Illuminate\Database\QueryException $e) {
            return back()->with('warning','No results found');
        }
        catch(\Exception $e){
            return back()->with('warning',$e->getMessage());
        }
    }

    
}
