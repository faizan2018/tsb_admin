<?php

namespace App\Services;


class ShipmentTracking
{
    //

    public function track($dock_no){
    	try{
    		$data = array(
        		'DocketNo' => "".$dock_no.""
    		);
    		$docket_data = $data;
            $headers = array('Content-Type: application/json');
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL,
            'https://instacom.dotzot.in/RestService/DocketTrackingService.svc/GetDocketTrackingDetails');
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode($docket_data) );
            $result = curl_exec($ch);
            curl_close($ch);
            $result1 = json_decode($result);
           // return dd($result1);
            //$status = array();
            if($result1[0]->Message != "Invalid Docket No."){
            	$tracking = \DB::table('tracking_codes')->where('Tracking Codes',$result1[0]->TRACKING_CODE)->first();
            if(isset($tracking->Description)){
                return json_encode([
                        "tracking_no" => $result1[0]->DOCKNO,
                        "status" => $tracking->Description
                    ]); 
                //return (array)$status;
            }
            	//return $tracking->Description;      
            else
            	return "Shipment details pushed, status will be available soon!";
            //return dd($result1[0]->TRACKING_CODE);
            }
            else{
                $tracking1 = \DB::table('manual_shipping')->where('tracking_no',$dock_no)->first();
                    //return "Hello";
                    return json_encode([
                        "tracking_no" => $tracking1->tracking_no,
                        "status" => $tracking1->status
                    ]); 
                    //return (array)$status;

                    //return "Invalid Docket/Tracking Number!";
            }
            
    	}
    	catch (\Illuminate\Database\QueryException $e) {
            return back()->with('warning','No results found');
        }
        catch(\Exception $e){
            return back()->with('warning',$e->getMessage());
        }	
    	
    }
}
