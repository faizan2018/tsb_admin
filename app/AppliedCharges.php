<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppliedCharges extends Model
{
    //
    protected $fillable = [
        'charge_name', 'charge_type', 'percent_value_exists', 'percent_value', 'charge_value', 'order_id',
    ];
}
