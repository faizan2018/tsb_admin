<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductSelection;
use App\ProductCatalog;
use App\ProductImage;
use App\Order;
use App\UserAttributesDetail;
use App\Shipping;
use App\Services\ShipmentTracking;
use App\AppliedCharges;
use App\PaymentModes;
use App\DeliveryDetail;
use App\Pricing;
use App\PaymentDetails;
use App\MiscellaneousCharge;
use App\Services\DeliverySystemService;

class OrdersController extends Controller
{
    //
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    //153
    //51
    public function listOrders(){
    	$product_selections = ProductSelection::select('*','orders.created_at as order_date')->join('user_attributes_details','product_selections.id','=','user_attributes_details.product_selections_id')->join('orders','product_selections.order_id','=','orders.id')->join('attribute_values','user_attributes_details.varchar_attr_value_id','=','attribute_values.id')->orderBy('order_date','desc')->get();
        $orders = array();
            foreach ($product_selections as $product_selection) {
                $delivery_details = DeliveryDetail::find($product_selection->delivery_details_id);
                $product_set = ProductCatalog::find($product_selection->product_id)->join('product_image_directories', 'product_catalogs.product_image_directory_id', '=', 'product_image_directories.id')->join('sub_category_versions', 'product_catalogs.version_id', '=', 'sub_category_versions.id')->first();
                $product = ProductCatalog::find($product_selection->product_id);
                $image = ProductImage::where('directory_id',$product_set->product_image_directory_id)->select('image_name')->first();
                $ship1 = Shipping::where('order_id',$product_selection->order_id)->first();
                $ship2 = \DB::table('manual_shipping')->where('order_id',$product_selection->order_id)->first();
                //return dd($ship2->tracking_no);
                $track = new ShipmentTracking;
                //$result = array();
                if(isset($ship1->order_id)){
                    $reason = $ship1->reason;
                    $result = $track->track($ship1->dock_no);
                    $result2 = json_decode($result,true);
                    $tracking_no = $result2['tracking_no'];
                    $status = $result2['status'];
                    $truth = true;
                }
                elseif(isset($ship2->order_id)){
                    $reason = "Shipment data has been succesfully pushed!";
                    $result = $track->track($ship2->tracking_no);
                    $result2 = json_decode($result,true);
                    $tracking_no = $result2['tracking_no'];
                    $status = $result2['status'];
                    $truth = true;
                }
                else{
                    $reason = "Shipping not set for this order yet !";
                    $truth = false;
                }
                $orders[] = array(
                    'order_date' => date('d, F Y', strtotime($product_selection->order_date)).', ' .date('h:i:s a',strtotime($product_selection->order_date)),
                    'total_amount' => $product_selection->total_order_price,
                    'ship_to' => $delivery_details->name,
                    'order_no' => '#TSB'.$product_selection->order_id,
                    'product_id' => $product->id,
                    'product_name' =>  $product->version->version_name,
                    'size' => $product_selection->varchar_field_value,
                    'quantity' => $product_selection->decimal_with_range_value,
                   // 'image' => $product->imageDir->directory_url.$image->image_name,
                    'contact' => $delivery_details->contact,
                    'address' => $delivery_details->address.', '.$delivery_details->city.', '.$delivery_details->state.', '.$delivery_details->country.'. '.$delivery_details->pincode,
                    'email' => $delivery_details->email,
                    'subtotal' => $product_selection->actual_price,
                    'tax' => 0,
                    'total' => $product_selection->total_order_price,
                    'unit_price' => $product_selection->actual_price,
                    'truth' => $truth,
                    'dock_no' => $tracking_no ?? '',
                    'status' => $status ?? '',
                    'reason' => $reason

                );
            }
              $clubbedOrders = array();
                $index = 0;
                $key = "order_no";

                foreach($orders as $order){
                    if($index == 0){
                        $flag = 0;
                        if(isset($prev_key)&&$order[$key]!=$prev_key){
                            $index++;
                            $flag = 1;
                        }
                        if($flag == 0)
                        $clubbedOrders[$index][] = $order;
                        $prev_key = $order[$key];
                    }
                    if($order[$key]!=$prev_key){
                        $index++; 
                    }
                    if($index != 0)
                        $clubbedOrders[$index][] = $order;
                    $prev_key = $order[$key];  
                }
        //return response($clubbedOrders);
    	return view('orders',compact('clubbedOrders'));
    }

    public function confirmOrder($order_id){
            $order = Order::findOrFail($order_id);
        try{
            $product_selection = ProductSelection::where('order_id',$order_id)->first();
            $whole_order_data = ProductSelection::where('order_id',$order_id)->get();
            $quantity = 0;
            $weight = 200;
            foreach ($whole_order_data as $single_product_data) {
                $user_attributes = UserAttributesDetail::where('product_selections_id',$single_product_data->id)->first();
                $quantity = $user_attributes->decimal_with_range_value; 
            }
            $weight = ($weight*$quantity)/1000;
            $pieces = ceil($quantity/2);
            //echo date('d/m/Y', strtotime('+7 days'));
            //return $product_selection->delivery_detail->address;
           // http://localhost:8010/confirmOrder/168
            $data = array(
                'Customer' => array(
                    'CUSTCD' => 'SP000102937'
                ),
                'DocketList' => array(
                    array(
                        'AgentID' => '',
                        'AwbNo' => '',
                        'Breath' => '5',
                        'CPD' => '12/12/2018',  //to be taken care of
                        'CollectableAmount' => "0",
                        'Consg_Number' => 'TSB'.$product_selection->order->order_id,
                        'Consolidate_EW' => '',
                        'CustomerName' => $product_selection->delivery_detail->name,
                        'Ewb_Number' => '',
                        'GST_REG_STATUS' => 'N',
                        'HSN_code' => '',
                        'Height' => '2.5',
                        'Invoice_Ref' => 'TSB'.$product_selection->order->order_id,
                        'IsPudo' => 'N',
                        'ItemName' => 'Socks '.$product_selection->order->order_id,
                        'Length' => '5',
                        'Mode' => 'P',
                        'NoOfPieces' => "".$pieces."",
                        'OrderConformation' => 'Y',
                        'OrderNo' => 'TSB'.$product_selection->order->order_id,
                        'ProductCode' => $product_selection->order->order_id,
                        'PudoId' => '',
                        'REASON_TRANSPORT' => '',
                        'RateCalculation' => 'N',
                        'Seller_GSTIN' => '',
                        'ShippingAdd1' => $product_selection->delivery_detail->address,
                        'ShippingAdd2' => $product_selection->delivery_detail->address,
                        'ShippingCity' => $product_selection->delivery_detail->city,
                        'ShippingEmailId' => $product_selection->delivery_detail->email,
                        'ShippingMobileNo' => $product_selection->delivery_detail->contact,
                        'ShippingState' => $product_selection->delivery_detail->state,
                        'ShippingTelephoneNo' => $product_selection->delivery_detail->contact,
                        'ShippingZip' => "".$product_selection->delivery_detail->pincode."",
                        'Shipping_GSTIN' => '',
                        'TotalAmount' => "".$product_selection->order->total_order_price."",
                        'TransDistance' => '',
                        'TransporterID' => '',
                        'TransporterName' => '',
                        'TypeOfDelivery' => 'Home Delivery',
                        'TypeOfService' => 'Economy',
                        'UOM' => 'Per KG',
                        'VendorAddress1' => '600, Sector 21, Panchkula',
                        'VendorAddress2' => '',
                        'VendorName' => 'The Socks Bakery',
                        'VendorPincode' => '134112',
                        'VendorTeleNo' => '8427299008',
                        'Weight' => "".$weight.""
                    )
                )
            );
            //return $product_selection;
            //return response($data);
            $Customer = $data;
           // return json_encode($Customer);
            $headers = array('Content-Type: application/json');
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL,
            'https://instacom.dotzot.in/RestService/PushOrderDataService.svc/PushOrderData_PUDO_GST' );
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode($Customer) );
            $result = curl_exec($ch);
            curl_close($ch);
            $result1 = json_decode($result);
            $shipping = new Shipping();
            $shipping->dock_no = $result1[0]->DockNo;
            $shipping->order_no = $result1[0]->OrderNo;
            $shipping->reason = $result1[0]->Reason;
            $shipping->succeed = $result1[0]->Succeed;
            $shipping->order_id = $product_selection->order->order_id;
            $shipping->save();
            return $result;

        }
        catch (\Illuminate\Database\QueryException $e) {
            return back()->with('warning','No results found');
        }
        catch(\Exception $e){
            return back()->with('warning',$e->getMessage());
        }
    }





    public function manualOrder(){
        $users = \DB::table('users')->get();
        $products = ProductCatalog::get();
        return view('manualOrder',compact('users','products'));
    }



    public function addOrder(Request $request){
        // $products = array();
        // $count=1;
        // foreach ($request->product as $item) {
        //     # code...
        //     $quantity = 'qty'.$count;
        //                 $products[] = array(
        //                     'product_id' => $item,
        //                     'quantity' => $request->$quantity
        //                 );
        //                 $count++;
        // }
        // return dd($products);
        // // return dd($request->name);

        // return dd($request->product);
        // $request->qty1;
        // $request->product;
        // $request->paymentId;
        // $request->amount;
        try {

            if (!$order = Order::orderBy('id', 'desc')->first()) {
                $order_number = 0000000001;
            } else {
                $order_number = $order->order_id + 1;
            }
            $payment = PaymentModes::find(1);
            $new_order = new Order();
            $new_order->order_id = $order_number;
            $payment_details = new PaymentDetails();
            $payment_details->payment_mode_id = $payment->id;
            $payment_details->payment_id = $request->paymentId;
            $payment_details->payment_amount = $request->amount;
            if ($payment_details->save()) {
                $new_order->payment_details_id = $payment_details->id;
                if ($new_order->save()) {
                    $total_price = 0;
                    $products = array();
                    $count=1;
                    foreach ($request->product as $item) {
                        # code...
                        $quantity = 'qty'.$count;
                        $products[] = array(
                            'product_id' => $item,
                            'quantity' => $request->$quantity
                        );
                        $count++;
                    }
                    foreach ($products as $item) {
                        $product_selection = new ProductSelection();
                        $product_selection->product_id = $item['product_id'];
                        $product = ProductCatalog::find($item['product_id']);
                        $now_available = ProductCatalog::find($product->id);
                        $now_available->stock = $product->stock - (1 * $item['quantity']);
                        $pricing = Pricing::find($product->id);
                        $product_selection->actual_price = $pricing->actual_price;
                        $product_selection->special_price = $pricing->special_price;
                        $product_selection->user_id = $request->name;
                        $delivery_details = DeliveryDetail::where('user_id', $request->name)->orderBy('id', 'desc')->first();
                        $product_selection->delivery_details_id = $delivery_details->id;
                        $product_selection->order_id = $new_order->id;
                        $product_selection->save();
                        $user_attributes = new UserAttributesDetail();
                        $user_attributes->text_attr_value_id = 1;
                        $user_attributes->varchar_attr_value_id = 4;
                        $user_attributes->decimal_with_range_id = 3;
                        $user_attributes->decimal_with_range_value = $item['quantity'];
                        $user_attributes->product_selections_id = $product_selection->id;
                        $user_attributes->save();
                        $total_price = $total_price + ($product_selection->actual_price * $item['quantity']);
                    }

                    $tax_charge = MiscellaneousCharge::find(1)->first();
                    // $delivery_charge = MiscellaneousCharge::find($request->delivery_charge_id);
                    $tax = new AppliedCharges();
                    //$delivery = new AppliedCharges();
                    $tax->charge_name = $tax_charge->charge_name;
                    $tax->charge_type = $tax_charge->charge_type;
                    $tax->percent_value_exists = $tax_charge->percent_value_exists;
                    $tax->percent_value = $tax_charge->percent_value;
                    $tax->order_id = $new_order->id;
                    // $delivery->charge_name = $delivery_charge->charge_name;
                    // $delivery->charge_type = $delivery_charge->charge_type;
                    // $delivery->percent_value_exists = $delivery_charge->percent_value_exists;
                    // $delivery->charge_value = $delivery_charge->charge_value;
                    // $delivery->order_id = $new_order->id;
                    if ($tax->save()) {
                       // $total_price = $total_price + ($tax->percent_value / 100) * $total_price;
                        if ($request->amount == $total_price) {
                            $final_order = Order::find($new_order->id);
                            $final_order->total_order_price = $total_price;
                            if ($payment->id == 1) {
                                $final_order->confirm = true;
                            } else {
                                $final_order->confirm = false;
                                $message = "You have choosen COD so you need to confirm your order";
                            }
                            if ($final_order->save()) {
                                //$deliverySystemService = new DeliverySystemService;
                                //$result = $deliverySystemService->confirmOrder($new_order->id);
                                $message = "Order successfully placed on behalf of user";
                                // if($request->razorpay){
                                //     $deliverySystemService = new DeliverySystemService;
                                //     $result = $deliverySystemService->confirmOrder($new_order->id);
                                // }
                                // $api = new Api($this->api_key, $this->api_secret);
                                // $senderEmail = 'support@thesocksbakery.com';
                                // $senderName = 'Support Team';
                                // $recepientEmail = $request->user()->email;
                                // $recepientName = $request->user()->name;
                                // $mailBody = $recepientName . ', your order bearing order no. #TSB' . $new_order->order_id . ' has been successfully placed';
                                // $mailSubject = 'Order successfully placed on The Socks Bakery';

                                // $senderEmail1 = 'support@thesocksbakery.com';
                                // $senderName1 = 'Support Team';
                                // $recepientEmail1 = 'pritika@thesocksbakery.com';
                                // $recepientName1 = 'Pritika Mehta';
                                // $mailBody1 = 'You have got an order on The Socks Bakery website, bearing order no. #' . $new_order->order_id . ' for ' . $recepientName1;
                                // $mailSubject1 = 'New Order (#' . $new_order->order_id . ') on The Socks Bakery';
                                // $mail = new MailingService;
                                // $mail1 = new MailingService;

                                // if ($mail->sendMail($senderEmail, $senderName, $recepientEmail, $recepientName, $mailSubject, $mailBody) && $mail1->sendMail($senderEmail1, $senderName1, $recepientEmail1, $recepientName1, $mailSubject1, $mailBody1)) {
                                //     if ($request->paypal) {
                                //         try {
                                //             $payment = Payment::get($request->payment_id, $this->_api_context);
                                //             $message = "Payment Successful";
                                //         } catch (PayPalConnectionException $e) {
                                //             $error_json = json_decode($e->getData(), 1);
                                //             print_r($error_json);
                                //             exit(1);
                                //         }
                                //     } elseif ($request->razorpay) {
                                //         $payment = $api->payment->fetch($request->payment_id)->capture(array('amount' => $request->amount * 100));
                                //         $message = "Payment Successful";
                                //     } else {
                                //         $message = "Payment unsuccessful";
                                //     }
                                // } else {
                                //     $message = "Order confirmed but message not sent !";
                                // }
                            } else {
                                $message = "Order not confirmed !";
                            }
                        } else {
                            $message = "Something is wrong with your payment amount";
                        }
                    } else {
                        $message = "Something is not good with the tax and delivery charge details";
                    }
                } else {
                    $message = "Something wrong went with the order";
                }
            } else {
                $message = "Something wrong in your payment details";
            }

            return back()->with('success',$message);
        } catch (\Exception $e) {
            return response($e->getMessage());
        }
    }
}
