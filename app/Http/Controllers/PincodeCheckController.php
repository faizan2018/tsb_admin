<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\PincodeCheck;

class PincodeCheckController extends Controller
{
    //
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function showInput(){
    	return view('pincode');
    }

    public function uploadPincodes(Request $request){
    	$this->validate($request,[
    			'pincode' => 'required|max:1024|mimes:csv,txt',
    		]);
    	try{
    	$path = Storage::disk('public')->put('Pincodes', $request->pincode);
	    	if(isset($path)){
	    		$myfile = fopen(storage_path().'/app/'.$path, "r") or die("Unable to open file!");
	    		while(($fileop = fgetcsv($myfile, 1000, ",")) !== false){
	    			$num = count($fileop);
	    			for ($c=0; $c < $num; $c++) {
	    					if($c%11 == 0){
								$fake = $fileop[$c];
	        					continue;
	        				}
	        				if($c%11 == 1){
	    						$pincode = new PincodeCheck();
	        					$pincode->product = $fileop[$c];
	        					continue;
	        				}
	        				if($c%11 == 2){
	        					$pincode->pincode = $fileop[$c];
	        					continue;
	        				}
	        				if($c%11 == 3){
	        					$pincode->city = $fileop[$c];
	        					continue;
	        				}
	        				if($c%11 == 4){
	        					$pincode->state = $fileop[$c];
	        					continue;
	        				}
	        				if($c%11 == 5){
	        					$pincode->region = $fileop[$c];
	        					continue;
	        				}
	    					if($c%11 == 6){
	    						if($fileop[$c] == 'Y')
	        					$pincode->prepaid = true;
	        					else
	        					$pincode->prepaid = false;
	        					continue;
	        				}
	    					if($c%11 == 7){
	    						if($fileop[$c] == 'Y')
	        					$pincode->cod = true;
	        					else
	        					$pincode->cod = false;	
	        					continue;
	        				}
	        				if($c%11 == 8){
	        					if($fileop[$c] == 'Y')
	        					$pincode->reversepickup = true;
	        					else
	        					$pincode->reversepickup = false;	
	        					continue;
	        				}
	        				if($c%11 == 9){
	        					if($fileop[$c] == 'Y')
	        					$pincode->pickup = true;
	        					else
	        					$pincode->pickup = false;
	        					continue;
	        				}
	        				if($c%11 == 10){
	        					$pincode->serviceable_by = $fileop[$c];
	        					$check = $pincode->save();
	        					continue;
	        				}		
	    			}
	    		}
	    		if($check == TRUE)
	    			return back()->with('success','File successfully imported!');
	    		else
	    			return back()->with('failure','File cannot be imported!');
	    	}
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','No results found');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }
}
