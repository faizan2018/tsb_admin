<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SetConfigController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function listConfigurations(){
    	return view('configs');
    }
}
