<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SubCategoryVersion;
use App\SubCategory;

class SubCategoryVersionController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listSubCategoryVersion(){
    	try{
    		$sub_category_versions = SubCategoryVersion::all();
    		$sub_categories = SubCategory::all();
	    	if($sub_category_versions ?? FALSE)
	    		return view('manageSubCategoryVersions', compact('sub_categories','sub_category_versions'));
	    		return view('manageSubCategoryVersions')->with('warning','Nothing to list here');
	    }
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','No results found');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}	
    }

    public function addSubCategoryVersion(Request $request){
    	try{
    		$request->validate([
    			'version_name' => 'required',
    			'sub_category_id' => 'required'
    		]);
    		$sub_category = new SubCategoryVersion;
    		$sub_category->version_name = $request->version_name;
    		$sub_category->sub_category_id = $request->sub_category_id;
    		if($sub_category->save())
    			return back()->with('success','You have successfully added a version name to sub category');
    			return back()->with('failure','Something went wrong');
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','No results found');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }

    public function editSubCategoryVersion($id){
    	try{
    		$sub_category_version = SubCategoryVersion::find($id);
    		$sub_category_versions = SubCategoryVersion::all();
    		$sub_categories = SubCategory::all();
    		if($sub_category_version ?? FALSE)
    			return view('manageSubCategoryVersions', compact('sub_category_version','sub_categories','sub_category_versions'));
    			return abort(404);
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','No results found');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }

    public function updateSubCategoryVersion(Request $request, $id){
    	try{
    		$request->validate([
    			'version_name' => 'required',
    			'sub_category_id' => 'required'
    		]);
    		$sub_category_version = SubCategoryVersion::find($id);
    		if($sub_category_version ?? FALSE){
    			$sub_category_version->version_name = $request->version_name;
    			$sub_category_version->sub_category_id = $request->sub_category_id;
    			if($sub_category_version->save())
    				return redirect('manageSubCategoryVersions')->with('success','Sub Category Version updated successfully');
    				return redirect('manageSubCategoryVersions')->with('failure','Sub Category Version cannot be updated');
    		}
    		else{
    			return abort(404);
    		}
    			
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','No results found');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }

    public function deleteSubCategoryVersion($id){
    	try{
    		$sub_category_version = SubCategoryVersion::find($id);
    		if($sub_category_version ?? FALSE)
    		{
    			if($sub_category_version->delete())
    				return redirect('manageSubCategoryVersions')->with('success','Sub category version deleted successfully');
    			else
    				return redirect('manageSubCategoryVersions')->with('failure','Sub category version cannot be deleted');

    		}
    		else{
    			return abort(404);
    		}				
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','Cannot delete this entry');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }
}
