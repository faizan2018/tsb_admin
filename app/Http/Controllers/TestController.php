<?php

namespace App\Http\Controllers;
use App\Services\ShipmentTracking;
use Illuminate\Http\Request;

class TestController extends Controller
{
    //
    public function track($dock_no){
    	$track = new ShipmentTracking;
    	$result = $track->track($dock_no);
    	return dd(json_decode($result));
    }

    public function orderTest(){
    	
    }
    
}
