<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SubCategory;
use App\Category;

class SubCategoryController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listSubCategory(){
    	try{
    		$sub_categories = SubCategory::all();
    		$categories = Category::all();
	    	if($sub_categories ?? FALSE)
	    		return view('manageSubCategories', compact('categories','sub_categories'));
	    		return view('manageSubCategories')->with('warning','Nothing to list here');
	    }
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','No results found');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}	
    }

    public function addSubCategory(Request $request){
    	try{
    		$request->validate([
    			'sub_category_name' => 'required',
    			'category_id' => 'required'
    		]);
    		$sub_category = new SubCategory;
    		$sub_category->sub_category_name = $request->sub_category_name;
    		$sub_category->category_id = $request->category_id;
    		if($sub_category->save())
    			return back()->with('success','You have successfully added a sub category');
    			return back()->with('failure','Something went wrong');
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','No results found');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }


    public function editSubCategory($id){
    	try{
    		$sub_category = SubCategory::find($id);
    		$sub_categories = SubCategory::all();
    		$categories = Category::all();
    		if($sub_category ?? FALSE)
    			return view('manageSubCategories', compact('sub_category','categories','sub_categories'));
    			return abort(404);
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','No results found');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }

    public function updateSubCategory(Request $request, $id){
    	try{
    		$request->validate([
    			'sub_category_name' => 'required',
    			'category_id' => 'required'
    		]);
    		$sub_category = SubCategory::find($id);
    		if($sub_category ?? FALSE){
    			$sub_category->sub_category_name = $request->sub_category_name;
    			$sub_category->category_id = $request->category_id;
    			if($sub_category->save())
    				return redirect('manageSubCategories')->with('success','Sub Category updated successfully');
    				return redirect('manageSubCategories')->with('failure','Sub Category cannot be updated');
    		}
    		else{
    			return abort(404);
    		}
    			
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','No results found');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }


    public function deleteSubCategory($id){
    	try{
    		$sub_category = SubCategory::find($id);
    		if($sub_category ?? FALSE)
    		{
    			if($sub_category->delete())
    				return redirect('manageSubCategories')->with('success','Sub category deleted successfully');
    			else
    				return redirect('manageSubCategories')->with('failure','Sub category cannot be deleted');

    		}
    		else{
    			return abort(404);
    		}				
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','Cannot delete this entry');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }
}
