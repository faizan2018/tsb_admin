<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AttributeType;
use App\AttributeName;
use App\Attribute_Set_Name;
use App\TextEntity;
use App\DecimalEntity;
use App\VarcharEntity;

class AttributeController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }



    //ATTRIBUTE TYPE CONTROLLERS


    public function addAttributeType(Request $request){
    	
    	try{
    		$request->validate([
    			'attribute_type' => 'required',
    			'attribute_field' => 'required',
    		]);
    		$attribute = new AttributeType;
    		$attribute->attribute_type = $request->attribute_type;
    		$attribute->field_type = $request->attribute_field;
    		if($attribute->save()){
    			return back()->with('success','Attribute type added successfully');
    		}
    		else{
    			return back()->with('failure','Attribute type cannot be added');
    		}
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','Nothin exists in this list');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }

    public function listAttributeType(){
    	try{
    		$attributes = AttributeType::all();
    		if($attributes[0] ?? FALSE)
    			return view('manageAttributes',compact('attributes'));
  				return view('manageAttributes')->with('warning','Nothing exists in this list');
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','No results found');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }

    public function editAttributeType($id){
    	try{
    		$attribute = AttributeType::find($id);
    		$attributes = AttributeType::all();
    		if($attribute ?? FALSE)
    			return view('manageAttributes',compact('attributes','attribute'));
    			return abort(404);
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','No such record found');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }

    public function updateAttributeType(Request $request, $id){
    	try{
    		$request->validate([
    			'attribute_type' => 'required',
    			'attribute_field' => 'required',
    		]);
    		$attribute = AttributeType::find($id);
    		if($attribute ?? FALSE){
    			$attribute->attribute_type = $request->attribute_type;
    			$attribute->field_type = $request->attribute_field;
    			if($attribute->update())
    				return redirect('attributeTypes')->with('success','You have successfully updated the attribute type');
    				return redirect('attributeTypes')->with('failure','This entry cannot be updated');
    		}
    		else{
    			return abort(404);
    		}
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','This attribute type cannot be updated');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }

    public function deleteAttributeType($id){
    	try{
    		$attribute = AttributeType::find($id);
    		if($attribute ?? FALSE){
    			if($attribute->delete())
    				return back()->with('success','You have successfully deleted the attribute type');
    				return back()->with('failure','This entry cannot be deleted');
    		}
    		else{
    			return abort(404);
    		}
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','This attribute type cannot be deleted');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }

    

    //ATTRIBUTE NAME CONTROLLERS


    public function addAttributeName(Request $request){
    	
    	try{
    		$request->validate([
    			'attribute_name' => 'required',
    			'attribute_type_id' => 'required',
    		]);
    		$attribute = new AttributeName;
    		$attribute->attribute_name = $request->attribute_name;
    		$attribute->attribute_type_id = $request->attribute_type_id;
    		if($attribute->save()){
    			return back()->with('success','Attribute name added successfully');
    		}
    		else{
    			return back()->with('failure','Attribute name cannot be added');
    		}
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','Nothing exists in this list');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }


    public function listAttributeName(){
    	try{
    		$attribute_names = AttributeName::all();
    		$attributes = AttributeType::all();
    		if($attributes[0] ?? FALSE)
    			return view('nameAttributes',compact('attribute_names','attributes'));
  				return view('nameAttributes')->with('warning','Nothing exists in this list');
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','No results found');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }

    public function editAttributeName($id){
    	try{
    		$attribute_name = AttributeName::find($id);
    		$attribute_names = AttributeName::all();
    		$attributes = AttributeType::all();
    		if($attribute_name ?? FALSE)
    			return view('nameAttributes',compact('attribute_names','attribute_name','attributes'));
    			return abort(404);
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','No such record found');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }

    public function updateAttributeName(Request $request, $id){
    	try{
    		$request->validate([
    			'attribute_name' => 'required',
    			'attribute_type_id' => 'required',
    		]);
    		$attribute_name = AttributeName::find($id);
    		if($attribute_name ?? FALSE){
    			$attribute_name->attribute_name = $request->attribute_name;
    			$attribute_name->attribute_type_id = $request->attribute_type_id;
    			if($attribute_name->update())
    				return redirect('attributeNames')->with('success','You have successfully updated the attribute name');
    				return redirect('attributeNames')->with('failure','This entry cannot be updated');
    		}
    		else{
    			return abort(404);
    		}
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','This attribute name cannot be updated');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }

    public function deleteAttributeName($id){
    	try{
    		$attribute_name = AttributeName::find($id);
    		if($attribute_name ?? FALSE){
    			if($attribute_name->delete())
    				return back()->with('success','You have successfully deleted the attribute name');
    				return back()->with('failure','This entry cannot be deleted');
    		}
    		else{
    			return abort(404);
    		}
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','This attribute name cannot be deleted');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }

    // ATTRIBUTE SET NAME CONTROLLER 

    public function addAttributeSetName(Request $request){
    	try{
    		if($request->existing != 'on'){
	    		$request->validate([
	    			'attribute_set_name' => 'required',
	    		]);
	    		$attribute_set_name = new Attribute_Set_Name;
	    		$attribute_set_name->attribute_set_name = $request->attribute_set_name;
	    		if($request->text_field == 'on')
	    			$attribute_set_name->check_text_field = true;
		    	else
		    		$attribute_set_name->check_text_field = false;
		    	if($request->varchar_field == 'on')
		    		$attribute_set_name->check_varchar_field = true;
		    	else
		    		$attribute_set_name->check_varchar_field = false;
		    	if($request->decimal_field == 'on')
		    		$attribute_set_name->check_decimal_field = true;
		    	else
		    		$attribute_set_name->check_decimal_field = false;
		    	if(!$attribute_set_name->save())
		    		return back()->with('failure','Attribute set name cannot be added');
	    	}
	    	else{
	    		$attribute_set_name = Attribute_Set_Name::find($request->exist_attribute_set_name)->first();
	    	}
	    		if($request->text_field == 'on'){
    				$text_entity = new TextEntity;
    				$text_entity->attribute_set_name_id = $attribute_set_name->id;
    				$text_entity->attribute_name_id = $request->text_attribute_name;
    				$text_entity->save();
	    		}

	    		if($request->varchar_field == 'on'){
	    			$varchar_entity = new VarcharEntity;
    				$varchar_entity->attribute_set_name_id = $attribute_set_name->id;
    				$varchar_entity->attribute_name_id = $request->varchar_attribute_name;
    				$varchar_entity->save();
	    		}

	    		if($request->decimal_field == 'on'){
	    			$text_entity = new DecimalEntity;
    				$text_entity->attribute_set_name_id = $attribute_set_name->id;
    				$text_entity->attribute_name_id = $request->decimal_attribute_name;
    				$text_entity->save();
	    		}

	    		if($text_entity->save()||$varchar_entity->save()||$text_entity->save())
	    			return back()->with('success','Attribute set name added successfully');
    			else
    				return back()->with('failure','Attribute set name cannot be added');
	    		
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','Nothing exists in this list');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    } 

    public function listAttributeSetName(){
    	try{
    		$attribute_set_names = Attribute_Set_Name::all();
    		$decimal_attribute_names = AttributeName::where('attribute_type_id',2)->get();
    		$varchar_attribute_names = AttributeName::where('attribute_type_id',3)->get();
    		$text_attribute_names = AttributeName::where('attribute_type_id',1)->get();
    		if($attribute_set_names ?? FALSE)
    			return view('attributeSetName',compact('attribute_set_names','decimal_attribute_names','text_attribute_names','varchar_attribute_names'));
  				return view('attributeSetName')->with('warning','Nothing exists in this list');
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','No results found');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }


    public function deleteAttributeSetName($id){
    	try{
    		$attribute_set_name = Attribute_Set_Name::find($id);
    		if($attribute_set_name ?? FALSE){
    			if($attribute_set_name->check_varchar_field == 1){
    				$varcharentities = VarcharEntity::where('attribute_set_name_id',$attribute_set_name->id);
    				$varcharentities->delete();
    			}
    			if($attribute_set_name->check_decimal_field == 1){
    				$decimalentities = DecimalEntity::where('attribute_set_name_id',$attribute_set_name->id);
    				$decimalentities->delete();
    			}
    			if($attribute_set_name->check_text_field == 1){
    				$textentities = TextEntity::where('attribute_set_name_id',$attribute_set_name->id);
    				$textentities->delete();
    			}
    			if($attribute_set_name->delete())
    				return back()->with('success','You have successfully deleted the attribute name');
    				return back()->with('failure','This entry cannot be deleted');
    		}
    		else{
    			return abort(404);
    		}
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','This attribute name cannot be deleted');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }



}
