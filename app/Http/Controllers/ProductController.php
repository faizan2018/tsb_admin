<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\Pricing;
use App\SEO;
use App\Coupon;
use App\ProductCatalog;
use App\ProductImage;
use App\ProductImageDirectory;
use App\AttributeName;
use App\SubCategoryVersion;
use App\Attribute_Set_Name;
use App\AttributeValues;

class ProductController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function manageProduct(){
    	$versions = SubCategoryVersion::all();
    	$attribute_set_names = Attribute_Set_Name::all();
    	return view('addProduct', compact('versions','attribute_set_names'));
    }

    public function addProduct(Request $request){

    	$pricing = new Pricing;
    	$pricing->actual_price = $request->actual_price;
		$pricing->special_price = $request->special_price;
		$pricing->special_price_start_date = $request->special_price_start;
		$pricing->special_price_end_date = $request->special_price_end;
		$pricing->save();

		\DB::table('s_e_os')->insert([
    		['title' => $request->seo_title, 'description' => $request->seo_description, 'generator' => $request->seo_generator, 'keywords' => $request->seo_keywords]
		]);
		$seo = \DB::table('s_e_os')->where('title',$request->seo_title)->first();

		$coupon = new Coupon;
		$coupon->coupon_code = $request->coupon_code;
		$coupon->coupon_worth = $request->coupon_worth;
		$coupon->coupon_start_date = $request->coupon_start_date;
		$coupon->coupon_end_date = $request->coupon_end_date;
		$coupon->save();

		$prod_image_dir = new ProductImageDirectory;
		$prod_image_dir->directory_url = 'storage/images/products/'.$request->product_name.'/';
		Storage::makeDirectory('public/images/products/'.$request->product_name.'/');
		$prod_image_dir->save();

		$files = $request->product_images;
		foreach($files as $prod_image){
			$product_image = new ProductImage;
			$product_image->image_name = $prod_image->getClientOriginalName();
			$product_image->directory_id = $prod_image_dir->id;
			$product_image->save();
			Storage::disk('public')->put('images/products/'.$request->product_name,$prod_image->getClientOriginalName());
		}
		
		$varchar_attribute_set_names = Attribute_Set_Name::select('attribute_name','varchar_entities.id as varchar_id')->join('varchar_entities','attribute__set__names.id','=','varchar_entities.attribute_set_name_id')->join('attribute_names','varchar_entities.attribute_name_id','=','attribute_names.id')->get();
    	$text_attribute_set_names = Attribute_Set_Name::select('attribute_name','text_entities.id as text_id')->join('text_entities','attribute__set__names.id','=','text_entities.attribute_set_name_id')->join('attribute_names','text_entities.attribute_name_id','=','attribute_names.id')->get();
    	$decimal_attribute_set_names = Attribute_Set_Name::select('attribute_name','decimal_entities.id as decimal_id')->join('decimal_entities','attribute__set__names.id','=','decimal_entities.attribute_set_name_id')->join('attribute_names','decimal_entities.attribute_name_id','=','attribute_names.id')->get();

			$data = array();
			foreach($text_attribute_set_names as $text){
				$data[] = array(
					'attribute_name' => $text->attribute_name,
					'entity_id' => $text->text_id,
					'entity_type' => 'text',
					'range' => false
				);
			}
			foreach($decimal_attribute_set_names as $decimal){
				$data[] = array(
					'attribute_name' => $decimal->attribute_name,
					'entity_id' => $decimal->decimal_id,
					'entity_type' => 'decimal',
					'range' => true
				);
			}
			foreach($varchar_attribute_set_names as $varchar){
				$data[] = array(
					'attribute_name' => $varchar->attribute_name,
					'entity_id' => $varchar->varchar_id,
					'entity_type' => 'varchar',
					'range' => false
				);
			}


			foreach($data as $tuple){
				$attrib = $tuple['attribute_name'];
				foreach($request->$attrib as $attrib_name){
					if($tuple['entity_type'] == 'text'){
						$text_entity = new AttributeValues;
						$text_entity->text_field_value= $attrib_name;
						$text_entity->text_entity_field_id = $tuple['entity_id'];
						$text_entity->save();
					}
					elseif($tuple['entity_type'] == 'decimal'){
						$decimal_entity = new AttributeValues;
						if($tuple['range'] == true){
							$decimal_entity->decimal_start_range = $request->decimal_start_range;
							$decimal_entity->decimal_end_range = $request->decimal_end_range;
						}
						else{
							$decimal_entity->decimal_field_value = $attrib_name;
						}
						$decimal_entity->decimal_entity_field_id = $tuple['entity_id'];
						$decimal_entity->save();
					}
					elseif($tuple['entity_type'] == 'varchar'){
						$varchar_entity = new AttributeValues;
						$varchar_entity->varchar_field_value= $attrib_name;
						$varchar_entity->varchar_entity_field_id = $tuple['entity_id'];
						$varchar_entity->save();
					}
				}
			}


    	$product_catalog = new ProductCatalog;
		$product_catalog->product_name = $request->product_name;
		$product_catalog->version_id = $request->product_version;
		$product_catalog->attribute_set_name_id = $request->attribute_set;
		$product_catalog->stock = $request->stock;
		$product_catalog->product_sequence_id = $request->product_sequence_id;
		$product_catalog->new_label_start_date = $request->product_new_label_start;
		$product_catalog->new_label_end_date = $request->product_new_label_end;
		$product_catalog->product_pricing_id = $pricing->id;
		$product_catalog->applicable_coupon_id = $coupon->id;
		$product_catalog->seo_id = $seo->id;
		$product_catalog->product_image_directory_id = $prod_image_dir->id;
		if($product_catalog->save())
			return back()->with('success','You have successfully added a product');
    		return back()->with('failure','Something went wrong');

    }
}
