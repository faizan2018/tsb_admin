<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactSolution;

class ContactController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function contactList(){
    	try{
	    	$contacts = ContactSolution::select('*','contact_solutions.updated_at as contact_solutions_updated', 'contact_solutions.id as solution_id')->join('contacts','contact_solutions.contact_form_id','=','contacts.id')->get();
	    	return view('displayContacts',compact('contacts'));
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','No results found');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    	
    }
}
