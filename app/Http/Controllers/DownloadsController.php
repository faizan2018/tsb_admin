<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class DownloadsController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function downloadcsv(Request $request){
    	try{
    		$time = date('H:i:s');
    		$path = storage_path('app/public/subscriberLists/SubscriberList-'.date('d-m-Y-'.$time).'.csv');
    		foreach($request->selected as $subscriber){
    			$subscribed = \DB::table('users')->where('id',$subscriber)->first();
    			
        		$myfile = fopen($path, "a") or die("Unable to open file!");
        		if($request->full_name == "on" && $request->email == "on" && $request->datetime == "on"){
        			$name = "\n".$subscribed->name."\t";
        			fwrite($myfile, $name);
        			$email = $subscribed->email;
        			fwrite($myfile, $email);
        			$datetime = "\t".date('d-m-Y',strtotime($subscribed->updated_at))
.' '.date('H:i:s',strtotime($subscribed->updated_at));
        			fwrite($myfile, $datetime);
        		}
        		
                if($request->full_name == "on" && $request->email == NULL && $request->datetime == NULL){
        			$name = "\n".$subscribed->name;
        			fwrite($myfile, $name);
        		}
        		
                if($request->full_name == "on" && $request->email == "on" && $request->datetime == NULL){
        			$name = "\n".$subscribed->name."\t";
        			fwrite($myfile, $name);
        			$email = "\n".$subscribed->email;
        			fwrite($myfile, $email);
        		}
        		
                if($request->full_name == "on" && $request->email == NULL && $request->datetime == "on"){
        			$name = "\n".$subscribed->name;
        			fwrite($myfile, $name);
        			$datetime = "\t".date('d-m-Y',strtotime($subscribed->updated_at))
.' '.date('H:i:s',strtotime($subscribed->updated_at));
        			fwrite($myfile, $datetime);
        		}
        		
                if($request->email == "on" && $request->full_name == NULL && $request->datetime == NULL){
        			$email = "\n".$subscribed->email;
        			fwrite($myfile, $email);
        		}
        		
                if($request->email == "on" && $request->full_name == NULL && $request->datetime == "on"){
        			$email = "\n".$subscribed->email;
        			fwrite($myfile, $email);
        			$datetime = "\t".date('d-m-Y',strtotime($subscribed->updated_at))
.' '.date('H:i:s',strtotime($subscribed->updated_at));
        			fwrite($myfile, $datetime);
        		}
        		
                if($request->datetime == "on" && $request->full_name == NULL && $request->email == NULL){
        			$datetime = "\n".date('d-m-Y',strtotime($subscribed->updated_at))
.' '.date('H:i:s',strtotime($subscribed->updated_at));
        			fwrite($myfile, $datetime);
        		}


        		fclose($myfile);
    		}
    		return redirect('/storage/subscriberLists/SubscriberList-'.date('d-m-Y-'.$time).'.csv');
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','No results found');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }
}
