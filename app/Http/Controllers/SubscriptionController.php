<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listSubscribers(){
    	$subscribers = \DB::table('users')->where('newsletter_subscribed',true)->orderBy('id','desc')->get();
    	return view('listSubscribers',compact('subscribers'));
    }
}
