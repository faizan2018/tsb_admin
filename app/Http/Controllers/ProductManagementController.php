<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductManagementController extends Controller
{
    //
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function listProductConfig(){
    	return view('manageProducts');
    }
}
