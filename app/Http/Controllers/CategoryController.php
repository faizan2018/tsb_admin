<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listCategory(){
    	try{
    		$categories = Category::all();
	    	if($categories[0] ?? FALSE)
	    		return view('manageCategories', compact('categories'));
	    		return view('manageCategories')->with('warning','Nothing to list here');
	    }
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','No results found');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}	
    }

    public function addCategory(Request $request){
    	try{
    		$request->validate([
    			'category_name' => 'required'
    		]);
    		$category = new Category;
    		$category->category_name = $request->category_name;
    		if($category->save())
    			return back()->with('success','You have successfully added a category');
    			return back()->with('failure','Something went wrong');
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','No results found');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }


    public function editCategory($id){
    	try{
    		$category = Category::find($id);
    		$categories = Category::all();
    		if($category ?? FALSE)
    			return view('manageCategories', compact('category','categories'));
    			return abort(404);
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','No results found');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }

    public function updateCategory(Request $request, $id){
    	try{
    		$request->validate([
    			'category_name' => 'required'
    		]);
    		$category = Category::find($id);
    		if($category ?? FALSE){
    			$category->category_name = $request->category_name;
    			if($category->save())
    				return redirect('manageCategories')->with('success','Category updated successfully');
    				return redirect('manageCategories')->with('failure','Category cannot be updated');
    		}
    		else{
    			return abort(404);
    		}
    			
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','No results found');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }


    public function deleteCategory($id){
    	try{
    		$category = Category::find($id);
    		if($category ?? FALSE)
    		{
    			if($category->delete())
    				return redirect('manageCategories')->with('success','Category deleted successfully');
    			else
    				return redirect('manageCategories')->with('failure','Category cannot be deleted');

    		}
    		else{
    			return abort(404);
    		}				
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','Cannot delete this entry');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }

}
