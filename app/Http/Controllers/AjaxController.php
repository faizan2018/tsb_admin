<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Attribute_Set_Name;
use App\TextEntity;
use App\VarcharEntity;
use App\DecimalEntity;

class AjaxController extends Controller
{
    //

    public function listAttributes(Request $request){
    	$attribute_set_name = Attribute_Set_Name::find($request->attribute_set_name)->first();
    	$data = array();
    	if($attribute_set_name->check_text_field ?? FALSE){
    		$text = TextEntity::all()->where('attribute_set_name_id',$attribute_set_name->id);
    		foreach ($text as $tuple) {
    			$data[] = array(
    				'attribute_name' => $tuple->textAttribName->attribute_name,
    				'range' => false,
    				'entity_id' => $tuple->id,
    				'entity_type' => 'text'
    			);
    		}
    	}
    	if($attribute_set_name->check_varchar_field ?? FALSE){
    		$varchar = VarcharEntity::all()->where('attribute_set_name_id',$attribute_set_name->id);
    		foreach ($varchar as $tuple) {
    			$data[] = array(
    				'attribute_name' => $tuple->varcharAttribName->attribute_name,
    				'range' => false,
    				'entity_id' => $tuple->id,
    				'entity_type' => 'varchar'  
    			);
    		}
    	}	
    	if($attribute_set_name->check_decimal_field ?? FALSE){
    		$decimal = DecimalEntity::all()->where('attribute_set_name_id',$attribute_set_name->id);
    		foreach ($decimal as $tuple) {
    			$data[] = array(
    				'attribute_name' => $tuple->decimalAttribName->attribute_name,
    				'range' => boolval($tuple->range_confirm),
    				'entity_id' => $tuple->id,
    				'entity_type'=> 'decimal'
    			);
    		}
    	}
    	return response($data);
    }
}
