<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactSolution;

class ContactSolutionController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addDescription(Request $request,$id){
    	try{
    		$contact_solution = ContactSolution::find($id);
    		if($contact_solution->admin_id == NULL){
    			$contact_solution->description = $request->description;
	    		$contact_solution->under_process = true;
	    		$contact_solution->admin_id = \Auth::user()->id;
	    		if($contact_solution->save())
	    			return back()->with('success','You have been successfully added as a handler of this contact.');
	    			return back()->with('failure','Sorry try again!');
    		}
    		return back()->with('failure','Sorry a bit late! You are not authorized to do so!');		
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','No results found');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    	
    }


    public function updateDescription(Request $request,$id){
    	
    	try{
    		$contact_solution = ContactSolution::find($id);
    		if(\Auth::user()->id == $contact_solution->admin_id){
    			$contact_solution->description = $request->description;
	    		if($contact_solution->save())
	    			return back()->with('success','You have successfully updated the description');
	    			return back()->with('failure','Sorry try again!');
    		}
    		return back()->with('failure','Sorry! You are not authorized to do so!');		
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','No results found');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }	

    public function releaseHandling(Request $request,$id){
    	
    	try{
    		$contact_solution = ContactSolution::find($id);
    		if(\Auth::user()->id == $contact_solution->admin_id){
    			$contact_solution->description = NULL;
    			$contact_solution->under_process = false;
    			$contact_solution->handled = false;
    			$contact_solution->admin_id = NULL;
	    		if($contact_solution->save())
	    			return back()->with('success','You have successfully released the contact form submission');
	    			return back()->with('failure','Sorry try again!');
    		}
    		return back()->with('failure','Sorry! You are not authorized to do so!');		
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','No results found');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }


    public function finalSubmitHandling(Request $request,$id){
    	
    	try{
    		$contact_solution = ContactSolution::find($id);
    		if(\Auth::user()->id == $contact_solution->admin_id){
    			$contact_solution->under_process = false;
    			$contact_solution->handled = true;
	    		if($contact_solution->save())
	    			return back()->with('success','You have successfully handled the contact form submission');
	    			return back()->with('failure','Sorry try again!');
    		}
    		return back()->with('failure','Sorry! You are not authorized to do so!');		
    	}
    	catch (\Illuminate\Database\QueryException $e) {
        	return back()->with('warning','No results found');
    	}
    	catch(\Exception $e){
    		return back()->with('warning',$e->getMessage());
    	}
    }
    	


}
