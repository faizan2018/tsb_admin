<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SEO extends Model
{
    //
    protected $fillable = [
        'title', 'description', 'generator', 'keywords'
    ];
}
